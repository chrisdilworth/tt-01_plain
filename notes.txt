CSS:
====
--Using live-css-compiler in vscode to auto-compile sass to /static/assets/css/styles.css (minified).
--To use 'npm run buildcss' (postcss+autoprefixer), point live-sass-compiler (in vscode settings) to
    /assets/css/, then run 'npm run buildcss'.

IMAGE COMPRESS:
===============
--Copy images into /images, then run 'node compress.js' which puts compressed images in /static/assets/img,
    then delete uncompressed images in /images.