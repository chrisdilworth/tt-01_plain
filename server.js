
const express = require('express')
const path = require('path');
const app = express()
const expressLayouts = require('express-ejs-layouts')
const PORT = process.env.PORT || 3000
const admins = require('./data/Admins')
const boards = require('./data/Boards')
const pastps = require('./data/Pastps')
const ttnews = require('./data/Ttnews')

app.use(express.static(path.join(__dirname, 'static')))

// Set the global variables
app.locals.siteTitleEn = "Talisman Theatre, Montreal &#8211; Creating English-language premieres of contemporary Quebec plays"
app.locals.siteTitleFr = "Talisman Théâtre, Montréal &#8211; Créer des premières en anglais de pièces québécoises contemporaines"
app.locals.pastps = pastps
app.locals.admins = admins
app.locals.boards = boards
app.locals.pastps = pastps
app.locals.ttnews = ttnews

// INIT MIDDLEWARE...
// ...ejs for templates
app.use(expressLayouts)
app.set('layout', './layouts/index')
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')
// ...body parser
app.use(express.json())
// ...for forms
app.use(express.urlencoded({extended: false}))

// For POST PUT DELETE methods, use joi to validate user input

// Std routes using ejs (see ./views/*)

// ...homepage
app.get('/index.html', (req, res) => {
  res.render('index');
})
// ...francais
app.get('/francais.html', (req, res) => {
  res.render('francais', {layout: './layouts/francais'});
})
// ... 404 has got to be the last route
app.use(function (req, res, next) {
  res.status(404).render('404')
})

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`)
})
