
const ttnews = [
  // {
  //   dates: "",,
  //   imags: {
  //     ima01: "",
  //   },
  //   titlE: "",
  //   titlF: "",
  //   textE: {
  //     par01: "",
  //   },
  //   textF: {
  //     par01: "",
  //   },
  // },

  {
    dates: "2021Q2",
    imags: {
      ima01: "lyne-at-desk-3.jpg",
    },
    titlE: "A word From the Artistic Director (at home)",
    titlF: "Un mot de la directrice artistique (de chez elle)",
    textE: {
      par01: "Hi friends!",
      par02: "Today I unveil Talisman's new digital strategy to produce a series of works for 2021-2022:",
      par04: "In the pipeline are two enhanced online readings: Sarah Berthiaume's <em>Antioch</em>, translated by Iris Cronin, directed by Sophie El Assaad; and Rachel Graton's <em>Night of the 4th to 5th</em>, translated by Katherine Turnbull, and directed by recent NTS graduate Isabelle Bartkowiak.",
      par05: "Digital versions of all of these plays, including last year's hit, <em>Habibi's Angels</em>, will be made available to schools as well as a Zoom Q&A with the artists.",
      par06: "Lyne Paquette",
      par07: "Artistic and Executive Director",
    },
    textF: {
      par01: "Salut les ami.e.s !",
      par02: "Aujourd'hui, je dévoile notre nouvelle stratégie numérique visant à produire une série d'œuvres pour 2021-2022 :",
      par04: "Nous diffuserons en ligne deux lectures en réalité augmentée : <em>Antioch</em> de Sarah Berthiaume, traduite par Iris Cronin et mise en scène par Sophie El Assaad ; et <em>Night of the 4th to 5th</em> de Rachel Graton, traduite par Katherine Turnbull et mise en scène par Isabelle Bartkowiak, récemment diplômée de l'ENT.",
      par05: "Des versions numériques de toutes ces pièces, y compris le succès de l'année dernière, <em>Habibi's Angels</em>, seront mises à la disposition des écoles, ainsi qu'une rencontre avec les artistes sur Zoom.",
      par06: "Lyne Paquette",
      par07: "Directrice artistique et exécutif",
    },
  },
  {
    dates: "2021Q2",
    imags: {
      ima01: "Heather_O'Neill.jpg",
    },
    titlE: "Heather is our next fundraiser guest of honour",
    titlF: "Heather est l'invitée d'honneur de notre prochaine soirée bénéfice",
    textE: {
      par01: "Heather O'Neill is a Montreal-born novelist, poet, short story writer, screenwriter and journalist. Her debut novel, <em>Lullabies for Little Criminals</em> (2006) won the 2007 edition of Canada Reads, the Hugh MacLennan Prize for Fiction, and was shortlisted for eight other major awards, including the Orange Prize for Fiction and the Governor General's Award. O'Neill was named by Chatelaine as one of the most influential women in Canada.",
    },
    textF: {
      par01: "Heather O'Neill est une romancière, poète, nouvelliste, scénariste et journaliste née à Montréal. Son premier roman, <em>Lullabies for Little Criminals</em> (2006), a remporté l'édition 2007 de Canada Reads, le prix Hugh MacLennan pour la fiction, et a été présélectionné pour huit autres prix importants, dont le prix Orange pour la fiction et le prix du Gouverneur général. O'Neill a été nommée par Chatelaine comme l'une des femmes les plus influentes du Canada.",
    },
  },
  {
    dates: "2021Q2",
    imags: {
      ima01: "artsvest.jpg",
      ima02: "business-arts.jpg",
      ima03: "heritage.jpg",
    },
    titlE: "Talisman /artsvest : a fruitful collaboration",
    titlF: "Talisman /artsvest : une relation fructueuse",
    textE: {
      par01: "We are most grateful to artsvest, Business/Arts and Canadian Heritage for the matching funds and for their mentorship during our 2020 sponsorship drive. We are happy to report that we exceeded our pre-approved amount by a large margin!",
    },
    textF: {
      par01: "Nous sommes très reconnaissants envers artsvest, Business/Arts et Patrimoine canadien pour les fonds de contrepartie et pour leur mentorat vers nos premières commandites en 2020. Nous sommes heureux d'annoncer que nous avons largement dépassé le montant préapprouvé !",
    },
  },
  {
    dates: "2021Q2",
    imags: {
      ima01: "CES_logotype018_RGB.jpg",
    },
    titlE: "Thanks Caisse d'économie solidaire",
    titlF: "Merci à la Caisse d'économie solidaire",
    textE: {
      par01: "A big thank you to the Caisse d'économie solidaire, the financial cooperative of collective enterprises and committed citizens, for its sponsorship of <em>Antioch</em>. To put your money to work for a social and sustainable economy: caissesolidaire.coop",
    },
    textF: {
      par01: "Un grand merci à la Caisse d'économie solidaire, la coopérative financière des entreprises collectives et des citoyens engagés, pour sa commandite d'<em>Antioch</em>. Pour mettre votre argent au service d'une économie sociale et durable : caissesolidaire.coop.",
    },
  },
  {
    dates: "2021Q2",
    imags: {
      ima01: "book_cover.jpg",
    },
    titlE: "A new history of Quebec theatre",
    titlF: "Une nouvelle Histoire du théâtre québécois",
    textE: {
      par01: "Talisman Theatre has made it into the history books! Specifically, we are mentioned in <a href='https://www.pum.umontreal.ca/catalogue/le_theatre_contemporain_au_quebec_1945_2015'><em>Le théâtre contemporain au Québec, 1945-2015 ; essai de synthèse historique et socio-esthétique</em></a>, on pages 483-484. Thanks to the editor, David Gilbert, for including us.",
    },
    textF: {
      par01: "Talisman Théâtre est entré dans les livres d'Histoire ! Plus précisément, nous sommes mentionnés dans <a href='https://www.pum.umontreal.ca/catalogue/le_theatre_contemporain_au_quebec_1945_2015'><em>Le théâtre contemporain au Québec, 1945-2015 ; essai de synthèse historique et socio-esthétique</em></a>, aux pages 483-484. Merci à l'éditeur, David Gilbert, de nous avoir inclus.",
    },
  },
  {
    dates: "2021Q2",
    imags: {
      ima01: "admin-naya.jpg",
    },
    titlE: "Welcome Naya!",
    titlF: "Bienvenue Naya !",
    textE: {
      par01: "Naya is a queer Lebanese-Canadian interdisciplinary artist. She is now based in Tiohtiá:ke as an uninvited guest and is always in the process of disidentification. They are interested in exploring questions of hope, recovery and transformation in a violent, patriarchal and colonial world. Theatre and performance are central to her practice. In recent years, they have focused on creating work that centres on the climate crisis, tackling the subject with intersectionality, exploring the positions of capitalism, xenophobia, racism and sexism in the crisis.",
    },
    textF: {
      par01: "Naya est un.e artiste interdisciplinaire libano-canadienne. Elle est maintenant basée à Tiohtiá:ke en tant que visiteur.e non invitée et est toujours en processus de désidentification. Iel s'intéresse à l'exploration des enjeux d'espoir, de réconstruction et de transformation dans un monde violent, patriarcal et colonial. Le théâtre et la performance sont au cœur de sa pratique. Ces dernières années, iel est focalisé.e sur la création d'œuvres centrées sur la crise climatique, abordant le sujet avec intersectionnalité, explorant la place du capitalisme, de la xénophobie, du racisme et du sexisme dans la crise.",
    },
  },
  {
    dates: "2021Q1",
    imags: {
      ima01: "lyne-at-desk-3.jpg",
      ima02: "habibi-team.jpg",
    },
    titlE: "A word From the Artistic Director (at home)",
    titlF: "Un mot du directeur artistique (chez elle)",
    textE: {
      par01: "Hi friends!",
      par02: "We successfully navigated a part of the chaos that the pandemic wrought for the performing arts. Our 2020 production, <em>Habibi’s Angels : Commission Impossible</em>, was developed (over Zoom), rehearsed, and performed (in-theatre) with a full, 24-woman, team of artists and designers all the while respecting social distancing protocols and precautions. Furthermore, it was a critical and popular success!",
      par03: "Quebec's theatres were closed on November 23rd, the day we were due to preview at La Chapelle, so we called in a 3-camera film crew, and adapted in-house surtitles to subtitles. We streamed on December the 2nd and 6th, and again, by popular demand, on December 27th and January 2nd. We are now making <em>Habibi's Angels</em> available to schools as streamed recordings and video on demand. This March, students at Vanier College and Concordia University will be watching.",
      par04: "Lyne Paquette",
      par05: "Artistic and Executive Director",
    },
    textF: {
      par01: "Salut les ami.e.s !",
      par02: "Nous avons réussi à naviguer une part du chaos que la pandémie a provoqué pour les arts du spectacle. Notre production 2020, <em>Habibi's Angels : Commission Impossible</em>, a été développée (sur Zoom), répétée et jouée (au théâtre) avec une équipe complète de 24 femmes, artistes et conceptrices, tout en respectant les protocoles et les précautions de distanciation sociale. De plus, ce fut un succès critique et populaire !",
      par03: "Les salles de théâtre du Québec étaient fermées le 23 novembre, le jour de l'avant-première à La Chapelle. Nous avons donc fait appel à une équipe de trois caméras, et adapté les surtitres maison aux sous-titres. Nous avons diffusé en continu les 2 et 6 décembre, puis, à la demande générale, les 27 décembre et 2 janvier. Nous mettons maintenant <em>Habibi's Angels</em> à la disposition des écoles sous forme d'enregistrements en streaming et de vidéo sur demande. En mars, les étudiants du Collège Vanier et de l'Université Concordia seront à l'écoute.",
      par04: "Lyne Paquette",
      par05: "Directrice artistique et exécutif",
    },
  },
  {
    dates: "2021Q1",
    imags: {
      ima01: "poster-2021_antioch.jpg",
    },
    titlE: "Our next production!",
    titlF: "Notre prochain spectacle !",
    textE: {
      par01: "We are excited to report that we have received a grant from CALQ under 'Exploration et déploiement numérique - 2020-2021' to create and produce the English-language premiere of <em>Antioch</em> by Sarah Berthiaume and translated by Iris Cronin. This is the perfect project for a society practicing social distancing in a global pandemic! <em>Antioch</em> is a play about three 'walled-in' women: Ines, a Syrian immigrant single mother; Jade, her first-generation Canadian daughter; and the most famous 'walled-in' woman of all time - Sophocles' Antigone. This will be a binaural production, providing our public with acoustic cues to build a mental landscape.",
    },
    textF: {
      par01: "Nous sommes ravis d'annoncer que nous avons reçu une subvention du CALQ dans le cadre du programme 'Exploration et déploiement numérique - 2020-2021' pour créer et produire la première en langue anglaise de <em>Antioch</em> de Sarah Berthiaume et traduit par Iris Cronin. C'est le projet parfait pour une société qui pratique la distanciation sociale dans une pandémie mondiale ! <em>Antioch</em> est une pièce sur trois femmes 'emmurées' : Ines, une mère célibataire immigrante syrienne ; Jade, sa fille canadienne de première génération ; et la femme 'emmurée' la plus célèbre de tous les temps - Antigone de Sophocle. Il s'agira d'une production binaurale, qui fournira à notre public des repères acoustiques pour construire un paysage mental.",
    },
  },
  {
    dates: "2021Q1",
    imags: {
      ima01: "admin-paul.jpg",
    },
    titlE: "A new Face on the Board",
    titlF: "Un nouveau visage au sein du Conseil",
    textE: {
      par01: "Paul Chambers is a visual artist and lighting/stage designer. From 2008 to 2013 Paul was the technical director of Tangente: Contemporary Movement Lab. His interest and collaboration in new projects has always stimulated his artistic journey. Since 2007, notably with studio 303, he leads pedagogical workshops for artists who would like to deepen their knowledge of lighting design. Paul is also a lecturer at Concordia University since 2015 in the Contemporary Dance department and a mentor at the National Theatre School of Canada.",
    },
    textF: {
      par01: "Paul Chambers est artiste visuel et concepteur lumière/scénique. De 2008 à 2013 Paul assume la direction technique de Tangente : Laboratoire de mouvements contemporains. Son intérêt et sa collaboration à de nouveaux projets ont toujours stimulé son parcours artistique. Depuis 2007, notamment avec le studio 303, il anime des ateliers pédagogiques destinés aux artistes qui voudraient approfondir leurs connaissances sur la conception lumière. Paul est aussi chargé de cours à l’Université Concordia depuis 2015 dans le département de Danse Contemporaine et il est mentor à l’École Nationale de Théâtre du Canada.",
    },
  },
  {
    dates: "2021Q1",
    imags: {
      ima01: "admin-masha.jpg",
    },
    titlE: "Welcome to the team!",
    titlF: "Bienvenue dans l'équipe !",
    textE: {
      par01: "Masha Bashmakova, our new intern, is a Montreal-based theatre artist from Russia and the UAE, navigating the ways in which creative expression serves as a deep and necessary language in expressing interpersonal and social-political challenges, conflicts and empathies. Her current practice spans visual and performing arts, with a specific focus in performance, theatre directing and interdisciplinary creation. In the last few years, Masha graduated from Concordia University’s Performance Creation program.",
    },
    textF: {
      par01: "Masha Bashmakova, notre nouvelle stagiaire, est une artiste de théâtre basée à Montréal, originaire de Russie et des Émirats arabes unis. Elle explore les façons dont l'expression créative sert de langage profond et nécessaire pour exprimer les défis interpersonnels et sociopolitiques, les conflits et les empathies. Sa pratique actuelle couvre les arts visuels et les arts de la scène, avec un accent particulier sur la performance, la mise en scène de théâtre et la création interdisciplinaire. Au cours des dernières années, Masha a obtenu son diplôme du programme de création performative de l'Université Concordia.",
    },
  },
  {
    dates: "2020Q3",
    imags: {
      ima01: "lyne.jpg",
    },
    titlE: "A word From the Artistic Director (at home)",
    titlF: "Un mot de la directrice artistique (en télétravail)",
    textE: {
      par01: "Hi friends!",
      par02: "The past months have been difficult for all of us with the lockdowns and social distancing forcing us into a new virtual reality. However, while we were on PAUSE, matters of great importance surfaced. Systemic discrimination, racism and sexism have been loudly denounced.",
      par03: "To the theatre students of Dawson College we say: We believe you and offer you our support, our energy, and our hearts. We are with you.",
      par04: "We have also taken the initiative to raise awareness of these issues by discussing them. Each month we shall welcome personalities--artists, activists, writers, etc.--who have used their platforms to denounce these systemic injustices. We held our first webinar on indigenous issues with guest speaker Nakuset in September.",
      par05: "We invite you to join us to share your stories and your culture, because all voices deserve to be heard. After each webinar, we will share resources such as books, podcasts, and articles.",
      par06: "Please take care of yourselves, and thank you for your continued loyalty and support.",
      par07: "Lyne Paquette",
      par08: "Artistic and Executive Director",
    },
    textF: {
      par01: "Salut les ami.e.s!",
      par02: "Les derniers mois ont été difficiles pour nous tous avec le confinement et la distanciation sociale nous obligeant à une nouvelle réalité virtuelle. Cependant, pendant que nous étions en PAUSE, des questions de grande importance ont fait surface. La discrimination systémique, le racisme et le sexisme ont été vivement dénoncés.",
      par03: "Aux étudiantes en théâtre du Collège Dawson nous disons que nous vous croyons et vous offrons notre soutien, notre énergie et notre cœur. Nous sommes avec vous.",
      par04: "Nous avons également pris l’initiative de faire de la sensibilisation à ces enjeux en les discutant. Chaque mois, nous accueillerons des personnalités - artistes, militantes, écrivaines, etc. - qui ont utilisé leurs plateformes pour dénoncer ces injustices systémiques. Nous avons organisé notre premier webinaire sur les enjeux autochtones avec la conférencière invitée Nakuset en septembre.",
      par05: "Nous vous invitons à vous joindre à nous pour partager vos histoires et votre culture, car toutes les voix méritent d'être entendues. Après chaque webinaire, nous partagerons des ressources telles que des livres, des balados et des articles.",
      par06: "Veuillez prendre soin de vous. Merci pour votre fidélité et votre soutien continus.",
      par07: "Lyne Paquette",
      par08: "Directrice artistique et exécutive",
    },
  },
  {
    dates: "2020Q3",
    imags: {
      ima01: "metas.jpg",
      ima02: "sophie-chadia.jpg",
    },
    titlE: "Congrats Sophie and Chadia!",
    titlF: "Félicitations à Sophie et Chadia !",
    textE: {
      par01: "Congratulations to Sophie Elassaad and Chadia Kikondjo for their META nominations! These talented women are part of Talisman's all-female production: <em>Habibi's Angels : Commission Impossible</em>. We send our congratulations to all META nominees--a little recognition goes a long way in difficult times. The awards ceremony will be on Sunday, November 8th and will be streamed live. 	",
    },
    textF: {
      par01: "Félicitations à Sophie Elassaad et Chadia Kikondjo pour leurs nominations au META! Ces femmes talentueuses font partie de la production entièrement féminine de Talisman: <em>Habibi's Angels: Commission Impossible</em>. Nous adressons nos félicitations à tous les nominés au META - un peu de reconnaissance va très loin dans les moments difficiles. La cérémonie de remise des prix aura lieu le dimanche 8 novembre et sera diffusée en direct.",
    },
  },
  {
    dates: "2020Q3",
    imags: {
      ima01: "ha-poster.jpg",
      ima02: "all-girls.jpg",
      ima03: "angels.jpg",
    },
    titlE: "Our next production",
    titlF: "Notre prochaine production",
    textE: {
      par01: "We are happy to announce that Talisman's first commissioned play is set to open on November 23rd at Theatre La Chapelle. In <em>Habibi's Angels : Commission Impossible</em>, an all-female cast tackles contemporary feminism and diversity.",
      par02: "Written by Kalale Dalton and Hoda Adra, directed by Sophie Gee, and choreographed by Claudia Chan Tak, <em>Habibi's Angels</em> stars Chadia Kikondjo, Aida Sabra, Lesly Velazquez, Emilee Veluz and France Rolland (voice).",
      par03: "We are settling into a new routine and new ways of making theatre for a socially distanced (limited) audience. Get your tickets while you can! Performances run from Nov 23 until Dec 5, 2020 at Theatre La Chapelle (COVID permitting). We will also be streaming the show on December 2nd--details to follow.",
      par04: "Synopsis: Profound, poetic, and deliciously quirky--The play is a living x-ray of Montreal that lays bare its foundations, ancient and modern. A meta-experiment on the collectivity with contrasting feminist visions. Audiences be warned: you're invited to a raw celebration of Montreal.",
    },
    textF: {
      par01: "Nous sommes heureux d'annoncer que la première commande de texte par Talisman ouvrira le 23 novembre au Théâtre La Chapelle. Dans <em>Habibi's Angel: Commission Impossible</em> on retrouve une distribution entièrement féminine abordant le féminisme contemporain et la diversité.",
      par02: "Écrit par Kalale Dalton et Hoda Adra, mise en scène par Sophie Gee et chorégraphie par Claudia Chan Tak, <em>Habibi's Angels</em> met en vedette Chadia Kikondjo, Aida Sabra, Lesly Velazquez, Emilee Veluz et France Rolland (voix).",
      par03: "Nous nous installons dans une nouvelle routine et de nouvelles façons de faire du théâtre pour un public(limité) en distanciation physique. Achetez vos billets tant que vous le pouvez! Les représentations se déroulent du 23 novembre au 5 décembre 2020 au Théâtre La Chapelle (si COVID le permet). Nous diffuserons également le spectacle le 2 décembre--détails à suivre.",
      par04: "Résumé : Profonde, poétique et délicieusement décalée - La pièce est une radiographie vivante de Montréal qui met à nu ses fondements, anciens et modernes. Une méta-expérience sur la collectivité aux visions féministes contrastées. Le public est prévenu: vous êtes invités à une célébration grinçante de Montréal.",
    },
  },
  {
    dates: "2020Q3",
    imags: {
      ima01: "admin-inass.jpg",
    },
    titlE: "An addition to the Talisman Team",
    titlF: "Un ajout à l'équipe Talisman",
    textE: {
      par01: "We would like to introduce you to our summer intern Inass Karin. Inass has a bachelor degree in landscape architecture and intends to specialize in sustainable developement. She has experience in event planning from her university years, and skills in customer services and content creation. Her passionate is for graphic rendering, cinema and photography. She is now discovering theatre with Talisman.",
    },
    textF: {
      par01: "Nous aimerions vous présenter notre stagiaire estivale Inass Karin. Inass est une étudiante fraichement graduée en Architecture de paysage, qui se spécialise dans les études en développement durable. Elle est passionnée par le design de rendu graphique, la photographie et le cinéma. Elle découvre le monde du théâtre par curiosité et amour pour l’art. Elle a de l’expérience dans l’organisation d’évènements, le service à la clientèle et la création de contenu. Aujourd’hui elle met ses connaissances au profit du théâtre Talisman.",
    },
  },
  {
    dates: "2020Q3",
    imags: {
      ima01: "alone.jpg",
    },
    titlE: "Talisman's 2020 fundraising campaign",
    titlF: "Campagne de financement Talisman 2020",
    textE: {
      par01: "We are launching our 2020 fundraising campaign. Talisman has been producing Quebec plays in translation for almost 15 years. Our numerous awards, including a META for 'Oustanding Ensemble' (<em>Clean Slate</em>, 2019), attest to the quality of our work. This year, we will be developing on-line readings and podcasts for you. This campaign will end on December 31, 2020 for those who would like to receive charitable donation tax receipts. You can make your donations at CanadaHelps.",
    },
    textF: {
      par01: "Nous lançons notre campagne de financement 2020. Talisman produit des pièces québécoises en traduction depuis près de 15 ans. Nos nombreux prix, dont un META pour 'Meilleur Ensemble' (<em>Clean Slate</em>, 2019), témoignent de la qualité de notre travail. Cette année, nous développerons pour vous des mises en lectures et des balados en ligne. Cette campagne se terminera le 31 décembre 2020 pour ceux qui souhaiteraient recevoir des reçus d'impôt pour dons de bienfaisance. Vous pouvez faire vos dons à CanaDon.",
    },
  },
  {
    dates: "2020Q2",
    imags: {
      ima01: "lyne-at-desk-2.jpg",
    },
    titlE: "A word From the Artistic Director (at home)",
    titlF: "Un mot du directeur artistique (chez elle)",
    textE: {
      par01: "Hi friends!",
      par02: "Isolation has some good points. Now that every day is clear of pollution I can sit here at my desk and look way out over the St. Lawrence to Mont Sutton. Looking into the future, however, is not so clear. But I remain optimistic and retain a firm grasp on Talisman's vision while taking advantage of both the simple life and advanced technology.",
      par03: "I miss bumping into you at theatre. I’m hoping to bridge the gap via social media, to find a way to revisit our favorite moments from past theatre productions. Send me a vivid memory, a flash connected to a past Talisman production. Share them on Facebook.",
      par04: "If you are drawing a blank, send us a message anyway on Facebook with any suggestions for how we might best serve you in these times. What do you expect for theatre in the next few months?",
      par05: "Talisman sends good vibes out to all of Montreal's essential workers and remains in solidarity with our artistic community turned upside down by this unforeseen and unpredictable situation. We can move forward only by continuing to act responsibly and creatively!",
      par06: "Please take care of yourselves, and thank you for your continued loyalty and support.",
      par07: "Lyne Paquette",
      par08: "Artistic and Executive Director",
    },
    textF: {
      par01: "Salut les ami.e.s !",
      par02: "L'isolement a quelques bons côtés : l’air plus pur me permet de m’assoir près de ma fenêtre et de voir au-delà du fleuve et même le Mont Sutton. Par contre, regarder vers l'avenir n'est pas aussi clair. Je reste cependant optimiste et je maintiens le cap quant à la vision de Talisman, tout en profitant de cette vie simple et sa technologie avancée.",
      par03: "Vous côtoyer au théâtre me manque, je cherche donc à nous rapprocher via les médias sociaux, un moyen pour revisiter nos moments préférés passés ensemble. Envoyez-moi un souvenir, un flash ou un moment inoubliable relié à une production antérieure de Talisman. Je la partagerai sur Facebook.",
      par04: "Si rien ne vous vient à l’esprit, envoyez-moi quand même un message sur Facebook avec vos idées quant à la manière dont Talisman peut vous servir au mieux en ces temps difficiles. Qu’attendez-vous du monde théâtral dans les prochains mois?",
      par05: "Talisman a une pensée pour tous les travailleuses essentiels et est solidaire avec les membres de la communauté artistique chamboulée par cette situation unique. Avançons ensemble, continuons d’agirde manière responsable et n’arrêtons surtout pas d’être créatif!",
      par06: "Prenez soin de vous et merci pour votre fidélité et votre soutien.",
      par07: "Lyne Paquette",
      par08: "Directrice artistique et exécutif",
    },
  },
  {
    dates: "2020Q2",
    imags: {
      ima01: "cake-30.jpg",
      ima02: "video.jpg",
    },
    titlE: "A word about the Talisman Team--Zoom On!!",
    titlF: "Un mot sur l’équipe Talisman",
    textE: {
      par01: "Talisman's Board of Directors and part-time staff are healthy and working in isolation from home. We are adapting our communications to the Zoom video conferencing platform. In April, the Board ratified our 2020-2021 communications and outreach strategies and discussed many other exciting projects.",
      par02: "Be sure to keep an eye out for the season launch at La Chapelle Theatre. They are celebrating their 30th anniversary this year!",
    },
    textF: {
      par01: "Le CA ainsi que le personnel à temps partiel chez Talisman sont en bonne santé et sont en mode télé-travail. Nous adaptons nos communications avec la plateforme de vidéoconférence Zoom. En avril, nous avons ratifié nos stratégies de communication et de sensibilisation, et discuté également de nombreux autres projets passionnants pour 2020-2021.",
      par02: "Ne manquez pas le lancement de la saison au théâtre La Chapelle, c’est le 30e anniversaire cette année !",
    },
  },
  {
    dates: "2020Q2",
    imags: {
      ima01: "zoom-workshop.jpg",
    },
    titlE: "Reading our work-in-progress on Zoom",
    titlF: "Nos travaux en cours sur Zoom",
    textE: {
      par01: "Talisman's next production is a commissioned play, written by Kalale Dalton and Hoda Adra. We performed a reading on Zoom earlier this month. These digital workshops with the feedback from our creative team are crucial to help us develop the final draft of this exciting work.",
      par02: "Profound, poetic, and deliciously quirky--The play is a living x-ray of Montreal that lays bare its foundations, ancient and modern. But can the invisible be made visible? A meta-experiment on the collectivity with contrasting feminist visions. Audiences be warned: you're invited to a raw celebration of Montreal.",
    },
    textF: {
      par01: "La prochaine production de Talisman Théâtre est une commande de texte, une co-écriture par Kalale Dalton et Hoda Adra. Une lecture du texte en chantier sur Zoom a été effectuée au début mai. Une première !! Ces ateliers numériques, avec les commentaires de notre équipe de création, sont essentiels pour nous aider à élaborer la version finale de cette œuvre passionnante.",
      par02: "Profonde, poétique et délicieusement décalée: La pièce est une radiographie vivante de Montréal qui met à nu ses fondations, anciennes et modernes. Mais l'invisible peut-il être rendu visible ? Une méta-expérience sur la collectivité avec des visions féministes contrastées. Mise en garde au public: vous êtes invités à une célébration grinçante de Montréal.",
    },
  },
  {
    dates: "2020Q2",
    imags: {
      ima01: "admin-emilie.jpg",
    },
    titlE: "A new Face on the Board",
    titlF: "Un nouveau visage au sein du Conseil",
    textE: {
      par01: "Talisman’s Board of Directors is pleased to welcome a new member: Emilie Rondeau.",
      par02: "Emilie holds a Master’s degree in Business Administration from McGill-HEC Montreal and a bachelor degree in biochemistry from Sherbrooke university. She has evolved in various industries over the past 20+ years, such as pharmaceutical, cosmetics and telecommunication and she has succeeded in managing different functions including business development, project management, marketing and fundraising. In the higher education field since 2016, she now acts as Director of Alumni Engagement and Annual Giving.",
    },
    textF: {
      par01: "Le conseil d'administration de Talisman est heureux d'accueillir un nouveau membre : Émilie Rondeau.",
      par02: "Émilie détient une maîtrise en administration des affaires de McGill-HEC Montréal ainsi qu’un baccalauréat en biochimie de l'université de Sherbrooke. Au cours de sa carrière, elle a évolué dans diverses industries: pharmaceutique, cosmétique et télécommunications, et a occupé différentes fonctions dont le développement des affaires, la gestion de projet, le marketing et la collecte de fonds. Dans le domaine de l'enseignement supérieur depuis 2016, elle est maintenant Directrice pour la mobilisation des diplômés et des dons annuels.",
    },
  },
  {
    dates: "2020Q2",
    imags: {
      ima01: "admin-claude.jpg",
    },
    titlE: "Thank you Claude!",
    titlF: "Merci Claude !",
    textE: {
      par01: "Talisman wishes to acknowledge outgoing Board member Claude St-Hilaire for his invaluable contribution over the past three years. We will sadly miss his enthusiastic participation in our Fundraising Committee meetings.",
    },
    textF: {
      par01: "Talisman tient à remercier Claude St-Hilaire, membre sortant du conseil d'administration, pour sa contribution inestimable au cours des trois dernières années. Sa participation enthousiaste aux réunions de notre comité de collecte de fonds nous manquera énormément.",
    },
  },
  {
    dates: "2020Q1",
    imags: {
      ima01: "admin-lyne-75x75.jpg",
    },
    titlE: "A word from our Artistic Director",
    titlF: "Un mot du Directeur artistique",
    textE: {
      par01: "Hi! I'm Lyne Paquette, the Artistic Director of Talisman Theatre bringing you our Winter news!",
    },
    textF: {
      par01: "Salut! Je suis Lyne Paquette, la directrice artistique du Talisman Théâtre, je vous présente l'infolettre d'hiver !",
    },
  },
  {
    dates: "2020Q1",
    imags: {
      ima01: "ba-blue-svg.svg",
    },
    titlE: "Your 2020 Sponsorships Matched Dollar-for-Dollar!",
    titlF: "Vos commandites 2020 seront doublées!",
    textE: {
      par01: "Your sponsorships will be matched by artsvest. What is artsvest? 'artsvest is Business / Arts signature mentorship training program designed to build capacity in Canada’s cultural sector. artsvest provides small to mid-sized arts and culture organizations with resources, expertise and training in marketing, board governance and sponsorship along with matching incentive funds and peer-to-peer networking.' To arrange a sponsorship, please click here to contact us.",
    },
    textF: {
      par01: "Vos commandites seront appuyées par les fonds de contrepartie d’artsvest. Qu'est-ce qu'artsvest? 'artvest est le programme principal de mentorat et de formation des Affaires / Arts et permet de développer la capacité du secteur culturel canadien. artsvest offre aux organisations artistiques et culturelles de petite et moyenne taille des ressources, une expertise et une formation en marketing, en gestion des conseils d’administration et en commandite ainsi que des fonds de contrepartie et des programmes de réseautage entre pairs.' Pour organiser un commandite, veuillez cliquez ici pour nous contacter.",
    },
  },
  {
    dates: "2020Q1",
    imags: {
      ima01: "kalale-hoda.jpg",
    },
    titlE: "Meet Our Playwrights",
    titlF: "Rencontrez nos autrices",
    textE: {
      par01: "We are proud to announce that Hoda Adra and Kalale Dalton-Lutale are the co-authors for our next production.",
      par02: "Kalale Dalton-Lutale is a Black queer writer and performer from Tkaronto/Toronto. Her work embraces surrealism, mothers, loss, and pop culture. At its core her work is always deeply curious about people. Currently Kalale is studying playwriting at The National Theatre School of Canada where she is developing a number of new works.",
      par03: "Hoda Adra writes, draws, and makes experimental videos. She was born in Lebanon, raised in Saudi Arabia and adopted by Montreal. Hoda is a proud alumna of the Banff Centre's Spoken Word Program, and her writing has been supported by the Canada Council for the arts and the Conseil des arts de Montreal. When she is not writing, Hoda can be found spilling paint.",
    },
    textF: {
      par01: "Nous sommes fiers d'annoncer que Hoda Adra et Kalale Dalton-Lutale sont les co-autrices de notre prochaine production.",
      par02: "Kalale Dalton-Lutale est une autrice et une interprète queer provenant de Tkaronto/Toronto. Son travail embrasse le surréalisme, les mères, la perte et la culture populaire. Au cœur de son travail, il y a toujours une profonde curiosité pour les gens. Kalale étudie présentement l'écriture dramatique à l'École nationale de théâtre du Canada où elle développe plusieurs nouvelles œuvres.",
      par03: "Hoda Adra écrit, dessine et réalise des vidéos expérimentales. Elle est née au Liban, a grandi en Arabie Saoudite et a été adoptée par Montréal. Hoda est fière d’avoir participé au programme de Spoken Word du Centre Banff, et ses oeuvres ont été soutenus par le Conseil des Arts du Canada et le Conseil des arts de Montréal. Lorsqu'elle n'écrit pas, on peut trouver Hoda en train de renverser de la peinture.",
    },
  },
  {
    dates: "2020Q1",
    imags: {
      ima01: "event2020-1.jpg",
      ima02: "event2020-2.jpg",
      ima03: "event2020-3.jpg",
    },
    titlE: "2020 Fundraiser Prizes",
    titlF: "Les Prix de la soirée bénéfice 2020",
    textE: {
      par01: "Team up with Talisman Theatre by sponsoring our annual fundraising event entitled 'Culture in Translation', February 19th, in the Conseil des arts de Montréal’s, Édifice Gaston-Miron. This is in support of our fall production. To arrange a sponsorship, please click here to contact us.",
      par02: "This year our guest of honour is Montreal’s own singer-songwriter Martha Wainwright who will be autographing her albums! She will talk about her decision to make Montreal her home. We will provide an open bar compliments of SAQ, canapés, an exciting live auction, readings from Talisman’s plays, and there will be lots of time to network.",
    },
    textF: {
      par01: "Faites équipe avec le Théâtre Talisman en commanditant notre événement annuel de collecte de fonds intitulé Culture en traduction``, le 19 février, à l'Édifice Gaston-Miron du Conseil des arts de Montréal. Cet événement vise à soutenir notre production d'automne. Pour organiser un commandite, veuillez cliquez ici pour nous contacter.",
      par02: "Cette année, notre invitée d'honneur est l'auteur-compositeur-interprète montréalaise Martha Wainwright qui signera ses albums! Elle nous parlera un peu de sa décision de vivre à Montréal. Nous offrirons un bar ouvert avec les compliments de la SAQ, des canapés, un encan en direct passionnant, des lectures d’extrait de textes par Talisman et nous aurons beaucoup de temps pour faire du réseautage.",
    },
  },
  {
    dates: "2020Q1",
    imags: {
      ima01: "tt-offices-s.jpg",
    },
    titlE: "Notices",
    titlF: "Avis",
    textE: {
      par01: "New Harrassment Policy: Talisman is updating its policies with the ever evolving anti-harassement legislation thanks to Board member Ada Sinacore. Click here to view this living document.",
      par02: "New office: Talisman is now 14 and growing fast. We are relocating to our new office in Shaughnessy Village. Lots of new adventures await us in this new decade. Stay tuned for our house warming party. Woot! Woot!!",
    },
    textF: {
      par01: "Nouvelle politique sur le harcèlement : Talisman met à jour ses politiques avec la législation anti-harcèlement en constante évolution grâce à Ada Sinacore, membre du conseil d'administration. Cliquez ici pour voir ce document évolutif.",
      par02: "Nouveau bureau : Talisman a maintenant 14 ans et grandit rapidement. Nous déménageons dans nos nouveaux bureaux dans le Village Shaughnessy. Beaucoup de nouvelles aventures nous attendent dans cette nouvelle décennie. Restez à l'écoute pour notre pendaison de crémaillère. Woot ! Woot !!",
    },
  },
]

module.exports = ttnews