const pastps = [

  // shw00: {
  //   pyear: "",
  //   dates: "",
  //   place: "",
  //   title: "",
  //   image: "",
  //   bannE: "",
  //   bannF: "",
  //   synop: {
  //     texts: {
  //       par01: "",
  //     },
  //   },
  //   auths: {
  //     aut01: {
  //       nameF: "",
  //       nameL: "",
  //     },
  //   },
  //   trans: {
  //     tra01: {
  //       nameF: "",
  //       nameL: "",
  //     },
  //   },
  //   drama: {
  //     dra01: {
  //       nameF: "",
  //       nameL: "",
  //     },
  //   },
  //   direc: {
  //     dir01: {
  //       nameF: "",
  //       nameL: "",
  //     },
  //   },
  //   chore: {
  //     cho01: {
  //       nameF: "",
  //       nameL: "",
  //     },
  //   },
  //   actor: {
  //     act01: {
  //       nameF: "",
  //       nameL: "",
  //       chara: "",
  //     },
  //   },
  //   desig: {
  //     des01: {
  //       nameF: "",
  //       nameL: "",
  //       title: "",
  //     },
  //   },
  //   copro: {
  //     texts: {
  //       par01: "",
  //     },
  //   },
  //   photo: {
  //     pho01: {
  //       nameF: "",
  //       nameL: "",
  //       image: "",
  //     },
  //   },
  //   criti: {
  //     cri01: {
  //       nameF: "",
  //       nameL: "",
  //       title: "",
  //       words: "",
  //       dates: "",
  //       links: "",
  //     },
  //   },
  //   logos: {
  //     log01: {
  //       image: "CCFA_RGB_colour_e.jpg",
  //     },
  //     log02: {
  //       image: "Calq_cyan.jpg",
  //     },
  //     log03: {
  //       image: "logo-cole.jpg",
  //     },
  //     log04: {
  //       image: "CMYK_Logo_CAM+Montreal.jpg",
  //     },
  //   },
  // },
  {
    pyear: "2020",
    dateE: "December 6 to January 2, 2021",
    dateF: "6 décember à 2 janvier, 2021",
    place: "YouTube",
    title: "Habibi's Angels",
    image: "poster-2020_ens_ha.jpg",
    bannE: "banner-2020_en_habibisangels.jpg",
    bannF: "banner-2020_fr_habibisangels.jpg",
    synop: {
      texts: {
        par01: "Profound, poetic, and deliciously quirky—The play is a living x-ray of Montreal that lays bare its foundations, ancient and modern. A meta-experiment on the collectivity with contrasting feminist visions. From its outermost shell--a volley of letters--to its innermost core—a plot to destroy The Machine—the play's true target is tokenism. All of this in a city that can't even align its streets with the four points of a compass! Audience be warned: you're invited to a scratchy celebration of Montreal.",
        par02: "The action begins when a pair of non-binary artists belonging to visible minorities are commissioned to write a bilingual play focusing on the themes of Quebec, feminism, history of women, love, and immigration... Nothing less! But one author complains: 'I'd rather die than write the multicultural feminist show. [...It] feels like I'm feeding some monster.' The other replies: 'I don’t wake up thinking: 'How diverse I feel!' [...] Let's blow up those categories.' And intersectional chaos seizes their writings!",
        par03: "The authors construct a labyrinthine system of characters and situations to trap and expose Habibi, a 'tentacular diversity-feminism-data-eating machine' that feeds off their content. Montreal is seen through a myriad of perspectives: the blocked writer surviving on coffee and cigarettes, the young immigrant resisting the established order, the elderly Arabic-speaker with her hands plunged in the earth, and a mischievous aquatic sprite that has more than one string to her bow. When Habibi finally imprisons their characters in her Diverse Play Factory, the authors manage to turn the tables causing her self-destruction.",
      },
    },
    auths: {
      aut01: {
        nameF: "Hoda",
        nameL: "Adra",
      },
      aut02: {
        nameF: "Kalale",
        nameL: "Dalton-Lutale",
      },
    },
    trans: {
      tra01: {
        nameF: "",
        nameL: "",
      },
    },
    drama: {
      dra01: {
        nameF: "Josianne",
        nameL: "Dulong-Savignac",
      },
    },
    direc: {
      dir01: {
        nameF: "Sophie",
        nameL: "Gee",
      },
    },
    chore: {
      cho01: {
        nameF: "Claudia",
        nameL: "Chan Tak",
      },
    },
    actor: {
      act01: {
        nameF: "Chadia",
        nameL: "Kikondjo",
        chara: "Water",
      },
      act02: {
        nameF: "Aida",
        nameL: "Sabra",
        chara: "Emma",
      },
      act03: {
        nameF: "Lesly",
        nameL: "Velazquez",
        chara: "Tamara",
      },
      act04: {
        nameF: "Emilee",
        nameL: "Veluz",
        chara: "Beth",
      },
      act05: {
        nameF: "France",
        nameL: "Rolland",
        chara: "Habibi's voice",
      },
    },
    desig: {
      des01: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Set",
        titlF: "Décor",
      },
      des02: {
        nameF: "Sophie",
        nameL: "El Assaad",
        titlE: "Costumes",
        titlF: "Costume",
      },
      des03: {
        nameF: "Leticia",
        nameL: "Hamaoui",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des04: {
        nameF: "Amelia",
        nameL: "Scott",
        titlE: "Video",
        titlF: "Vidéo",
      },
      des05: {
        nameF: "Maria Elena",
        nameL: "Stoodley",
        titlE: "Sound",
        titlF: "Son",
      },
      des06: {
        nameF: "Emmanuelle",
        nameL: "Brousseau",
        titlE: "Production Manager",
        titlF: "Gestionnaire de production",
      },
      des07: {
        nameF: "Alessandra",
        nameL: "Tom",
        titlE: "Stage Manager",
        titlF: "Régisseur",
      },
      des08: {
        nameF: "Sophie",
        nameL: "Bergeron",
        titlE: "Technical director",
        titlF: "Directrice technique",
      },
      des09: {
        nameF: "Zoé",
        nameL: "Roux",
        titlE: "Assistant Designer",
        titlF: "Assistance concepteur",
      },
    },
    copro: {
      texts: {
        par01: "",
      },
    },
    photo: {
      pho01: {
        nameF: "Maxime",
        nameL: "Côté",
        image: "2020-ha-4609.jpg",
      },
      pho02: {
        nameF: "Maxime",
        nameL: "Côté",
        image: "2020-ha-4774.jpg",
      },
      pho03: {
        nameF: "Maxime",
        nameL: "Côté",
        image: "2020-ha-4934.jpg",
      },
      pho04: {
        nameF: "Maxime",
        nameL: "Côté",
        image: "2020-ha-5259.jpg",
      },
      pho05: {
        nameF: "Maxime",
        nameL: "Côté",
        image: "2020-ha-5300.jpg",
      },
      pho06: {
        nameF: "Maxime",
        nameL: "Côté",
        image: "2020-ha-5348.jpg",
      },
      pho07: {
        nameF: "Maxime",
        nameL: "Côté",
        image: "2020-ha-5372.jpg",
      },
      pho08: {
        nameF: "Maxime",
        nameL: "Côté",
        image: "2020-ha-5578.jpg",
      },
      pho09: {
        nameF: "Maxime",
        nameL: "Côté",
        image: "2020-ha-5581.jpg",
      },
      pho10: {
        nameF: "Maxime",
        nameL: "Côté",
        image: "2020-ha-5591.jpg",
      },
      pho11: {
        nameF: "Maxime",
        nameL: "Côté",
        image: "2020-ha-5629.jpg",
      },
      pho12: {
        nameF: "Maxime",
        nameL: "Côté",
        image: "2020-ha-5874.jpg",
      },
      pho13: {
        nameF: "Maxime",
        nameL: "Côté",
        image: "2020-ha-5611.jpg",
      },
    },
    criti: {
      cri01: {
        nameF: "Daphnée",
        nameL: "Bathalon",
        title: "MonTheatre",
        words: "In directing, Sophie Gee overcomes the difficulties of distancing herself from the stage; her artistic direction is well served by the original costume design by Sophie El Assaad and the dynamic choreography by Claudia Chan Tak. The production also features performers Chadia Kikondjo, Aida Sabra, Lesly Velazquez and Emilee Veluz, who demonstrate an overwhelming energy, especially Sabra in the role of the old Arabic-speaking neighbor.",
        dates: "December 07, 2020",
        links: "20201207-ha-montheatre.pdf",
      },
      cri02: {
        nameF: "Nathalie",
        nameL: "de Han",
        title: "La Scena",
        words: "<em>Habibi's Angels: Commission Impossible</em> highlights the fact that it is still difficult for a newcomer to integrate even though visible minority artists have become fashionable. While the play evokes the Law on State Secularism (Law 21), it does not claim to be a pamphleteer. It is just a tragicomic fable, a bit pungent, in which many female spectators and, why not, some male spectators will recognize themselves.",
        dates: "December 4, 2020",
        links: "20201204-ha-lascena.pdf",
      },
      cri03: {
        nameF: "Mario",
        nameL: "Cloutier",
        title: "JEU Revue de théâtre",
        words: "...Sophie Gee's staging is inventive despite the social distancing, the four performers... frankly energetic, the scenography, simple and effective, the choreography by Claudia Chan Tak, quality, the use of video, very welcome, and the costumes by Sophie El Assaad, delirious. Their fun mix of multicultural motifs fused with a comic book aesthetic in a 'superhero Power Rangers' style is worth mentioning.",
        dates: "December 01, 2020",
        links: "20201202-ha-revuejeu.pdf",
      },
      cri04: {
        nameF: "Mario",
        nameL: "Cloutier",
        title: "En Toutes Lettres",
        words: "In a society where a Tokenist Quebec government--making symbolic efforts to include minority groups in order to escape accusations of discrimination--refuses to acknowledge the existence of systemic racism, <em>Habibi</em> sheds a refreshing light.",
        dates: "November 26, 2020",
        links: "20201126-ha-cloutier.pdf",
      },
      cri05: {
        nameF: "Sarah",
        nameL: "Dehaies",
        title: "CKUT Upstage",
        words: "Sophie Gee (interviewee): 'So here we have the Angels who are--you'll see from the video because I don't think we'll be able to perform it live in front of an audience--but they are a play on women of colour; they have super, super, super bright clothing on, and they are a visible minority, and they've been tasked as these superheroes by this unknown person, Habibi, to make a play, to capitalize on the fact that they are women of colour. ...And, actually, ... the character of Habibi is trying to get trauma porn.'",
        dates: "November 26, 2020",
        links: "20201126-ha-ckut.pdf",
      },
      cri06: {
        nameF: "Sarah-Louise",
        nameL: "Pelletier-Morin",
        title: "Spirale",
        words: "The voice of Hoda, whom I reached by phone on November 20, is joyful ... 'It's a meta-theatrical piece,' she sums up, at last. I immediately realize that, without realizing it, we were actually talking about the play throughout the interview. Because the show comes to ask the same question that nourished our exchanges: how to put forward alternative stories about Montreal, without art becoming number painting?",
        dates: "November 25, 2020",
        links: "20201125-ha-spirale.pdf",
      },
      cri07: {
        nameF: "Catherine",
        nameL: "Lalonde",
        title: "LeDevoir",
        words: "Sophie Gee, director of <em>Habibi's Angels</em> (Talisman Theatre), also takes things with perspective. ... 'I think we can all benefit from calming down, from living on a fallow land, from refocusing on processes rather than on their products. Let's use this time in the theatre as an additional residency, which is a great artistic luxury'.",
        dates: "November 21, 2020",
        links: "20201121-ha-ledevoir.pdf",
      },
    },
    logos: {
      log01: {
        image: "CCFA_RGB_colour_e.jpg",
      },
      log02: {
        image: "Calq_cyan.jpg",
      },
      log03: {
        image: "logo-cole.jpg",
      },
      log04: {
        image: "CMYK_Logo_CAM+Montreal.jpg",
      },
    },
  },
  {
    pyear: "2019",
    dateE: "March 18-30, 2019",
    dateF: "18-30 Mars, 2019",
    place: "La Chapelle Theatre",
    title: "Clean Slate",
    image: "poster-2019_en_cs.jpg",
    bannE: "banner-2018_en_cleanslate.jpg",
    bannF: "banner-2018_fr_cleanslate.jpg",
    synop: {
      texts: {
        par01: "Six friends gather at a site of their shared childhood: They drink, they laugh, but most importantly, they admit things they’ve never before had the courage to say. Inspired by the seemingly incomprehensible decision of their friend, the girls make a pact to shed their traumas and bad habits and start afresh. With the honesty and intimacy of a death bed, <em>Clean Slate</em> allows the audience to eavesdrop on a brave, messy, and above all authentic portrait of millennial sisterhood.",
        par02: "This translation is commissioned and developed by Playwrights' Workshop Montreal's Cole Foundation Emerging Translator program.",
      },
    },
    auths: {
      aut01: {
        nameF: "Catherine",
        nameL: "Chabot",
      },
      aut02: {
        nameF: "Brigitte",
        nameL: "Poupart",
      },
      aut03: {
        nameF: "Vicky",
        nameL: "Bertrand",
      },
      aut04: {
        nameF: "Marie-Anick",
        nameL: "Blais",
      },
      aut05: {
        nameF: "Rose-Anne",
        nameL: "Déry",
      },
      aut06: {
        nameF: "Sarah",
        nameL: "Laurendeau",
      },
      aut07: {
        nameF: "Marie-Noëlle",
        nameL: "Voisin",
      },
    },
    trans: {
      tra01: {
        nameF: "Jennie",
        nameL: "Herbin",
      },
    },
    drama: {
      dra01: {
        nameF: "Maureen",
        nameL: "Labonté",
      },
    },
    direc: {
      dir01: {
        nameF: "Leslie",
        nameL: "Baker",
      },
    },
    chore: {
      cho01: {
        nameF: "",
        nameL: "",
      },
    },
    actor: {
      act01: {
        nameF: "Cleopatra",
        nameL: "Boudreau",
        chara: "Marie-Anick",
      },
      act02: {
        nameF: "Rebecca",
        nameL: "Gibian",
        chara: "Marie-Noelle",
      },
      act03: {
        nameF: "Gita",
        nameL: "Miller",
        chara: "Vicky",
      },
      act04: {
        nameF: "Michelle",
        nameL: "Langlois-Fequet",
        chara: "Catherine",
      },
      act05: {
        nameF: "Kathleen",
        nameL: "Stavert",
        chara: "Rose",
      },
      act06: {
        nameF: "Julie",
        nameL: "Trepanier",
        chara: "Sarah",
      },
      act07: {
        nameF: "Christian",
        nameL: "Daoust",
        chara: "Delivery boy",
      },
    },
    desig: {
      des01: {
        nameF: "Peter",
        nameL: "Bottazzi",
        titlE: "Set",
        titlF: "Décor",
      },
      des02: {
        nameF: "Sophie",
        nameL: "El Assaad",
        titlE: "Costumes",
        titlF: "Costume",
      },
      des03: {
        nameF: "Cédric",
        nameL: "Delorme-Bouchard",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des04: {
        nameF: "Peter",
        nameL: "Cerone",
        titlE: "Sound",
        titlF: "Son",
      },
      des05: {
        nameF: "Isabel",
        nameL: "Faia",
        titlE: "Stage Manager",
        titlF: "Régisseur",
      },
      des06: {
        nameF: "Katey",
        nameL: "Wattam",
        titlE: "Assistant Director",
        titlF: "Assistant m-e-s",
      },
    },
    copro: {
      texts: {
        par01: "",
      },
    },
    photo: {
      pho01: {
        nameF: "",
        nameL: "",
        image: "2019-cs-2808.jpg",
      },
      pho02: {
        nameF: "",
        nameL: "",
        image: "2019-cs-3204.jpg",
      },
      pho03: {
        nameF: "",
        nameL: "",
        image: "2019-cs-3208.jpg",
      },
      pho04: {
        nameF: "",
        nameL: "",
        image: "2019-cs-3274.jpg",
      },
      pho05: {
        nameF: "",
        nameL: "",
        image: "2019-cs-3974.jpg",
      },
      pho06: {
        nameF: "",
        nameL: "",
        image: "2019-cs-4014.jpg",
      },
    },
    criti: {
      cri01: {
        nameF: "Jim",
        nameL: "Burke",
        title: "Theatre Funhouse",
        words: "The first-rate cast... give magnificently loose-limbed yet precisely executed performances, while Leslie Baker's fluid direction creates order out of the potential chaos of the boozy long day’s journey into night. ...What’s certain is that this is a beautifully staged ensemble piece that offers a refreshingly raw portrait of female friendship.",
        dates: "Mar 26, 2019",
        links: "2019-cs-theatrefunhouse.pdf",
      },
      cri02: {
        nameF: "Nancie",
        nameL: "Boulay",
        title: "ARP.MEDIA",
        words: "The chemistry between the actresses zings. It feels like attending a real girls' dinner, witnessing real conversations. The directing is excellent. ...<em>Clean Slate</em> is a funny, moving and resolutely feminist work that speaks of friendship, feminine solidarity and unconditional love.",
        dates: "Mar 25, 2019",
        links: "2019-cs-arpmedia.pdf",
      },
      cri03: {
        nameF: "Yanik",
        nameL: "Comeau",
        title: "Théâtralités",
        words: "This incisive, hard-hitting, unrestrained dialogue of a 30-year-old author passes readily into English. ...Leslie Baker orchestrates a magnificent sextet of actresses who play Marie-Anick, Marie-Noëlle, Catherine, Vicky, Rose and Sarah as if the characters were written especially for them.",
        dates: "Mar 24, 2019",
        links: "2019-cs-theatralites.pdf",
      },
      cri04: {
        nameF: "Alexane",
        nameL: "Anglehart",
        title: "Le Culte",
        words: "Inside each character, we find a little pieces of ourselves, sketches of our hopes, our dreams and our anxieties ... Watching this play is the equivalent of looking into the other, to get along and maybe even better understand the other to better understand oneself, but also to understand more about those around us.",
        dates: "Mar 21, 2019",
        links: "2019-cs-leculte.pdf",
      },
      cri05: {
        nameF: "Andrew",
        nameL: "Sawyer",
        title: "Montreal Theatre Hub",
        words: "Honest and resolute, <em>Clean Slate</em> shines in English premiere: Good art is a splash of cold water on the face; it reinvigorates the viewer. ...<em>Clean Slate</em> is modern theatre at its best: timely and well-rounded. I cannot speak highly enough of Talisman’s latest production. The show runs until March 30th and you’d be foolish to miss it.",
        dates: "Mar 20, 2019",
        links: "2019-cs-mth.pdf",
      },
      cri06: {
        nameF: "Sarah",
        nameL: "Deshaies",
        title: "CULT #MTL",
        words: "I thought to myself: these women are shrieking harpies. ...But, I realized I’ve spewed as much invective as a young woman. ...See <em>Clean Slate</em> with your girlfriends. Make sure to grab some wine before you head in.",
        dates: "Mar 20, 2019",
        links: "2019-cs-cultmtl.pdf",
      },
      cri07: {
        nameF: "Thomas",
        nameL: "Duret",
        title: "Arts et culture",
        words: "What would we do if we only had one night to live with our friends or our family? After all, it is a universal theme that deserves to be approached from different perspectives, and in this the text hits the mark. ...The scenography offers beautiful images and launches us on some tracks quite open to interpretation.",
        dates: "Mar 19, 2019",
        links: "2019-cs-artsetculture.pdf",
      },
    },
    logos: {
      log01: {
        image: "CCFA_RGB_colour_e.jpg",
      },
      log02: {
        image: "Calq_cyan.jpg",
      },
      log03: {
        image: "logo-cole.jpg",
      },
      log04: {
        image: "CMYK_Logo_CAM+Montreal.jpg",
      },
      log04: {
        image: "cs-logo-IIC-colore-montreal-sm.jpg",
      },
    },
  },
  {
    pyear: "2017",
    dateE: "November 21 to December 2, 2017",
    dateF: "21 novembre à 2 décembre, 2017",
    place: "The Centaur Theatre",
    title: "Vic and Flo",
    image: "poster-2017_en_Vic-and-Flo.jpg",
    bannE: "banner-2017_en_Vic-and-Flo.jpg",
    bannF: "banner-2017_fr_Vic-and-Flo.jpg",
    synop: {
      texts: {
        par01: "In <em>Vic and Flo</em>, Victoria is forty-six and on parole after serving most of a life-sentence for murder. She’s in love with Flo, a younger ex-con who comes to stay with her in her uncle's sugar shack located in the densely forested boondocks of Quebec. But these ex-con lesbian lovers begin to feel under siege from Vic's nosy parole officer and a mysterious woman from the woods who casts an increasingly menacing shadow from Flo's past. As Vic and Flo hurtle toward their fates the buccolic narrative tapestry slowly unravels before being violently torn to shreds.",
        par02: "Talisman is breaking new ground this fall when it produces the world stage premiere of Denis Côté’s film <em>Vic and Flo ont vu un ours</em>. A genre bending horror/love story, <em>Vic and Flo</em> won numerous international awards including the prestigious Berlin Film Festival Alfred Bauer award for 'opening new perspectives on cinematic art.'",
        par03: "The translation and adaptation of <em>Vic and Flo</em> was developed through Playwrights' Workshop Montréal's The Glassco Translation Residency in Tadoussac and their Translation Workshop Program.",
      },
    },
    auths: {
      aut01: {
        nameF: "Denis",
        nameL: "Côté",
      },
    },
    trans: {
      tra01: {
        nameF: "Michael",
        nameL: "Mackenzie",
      },
    },
    drama: {
      dra01: {
        nameF: "",
        nameL: "",
      },
    },
    direc: {
      dir01: {
        nameF: "Michael",
        nameL: "Mackenzie",
      },
    },
    chore: {
      cho01: {
        nameF: "",
        nameL: "",
      },
    },
    actor: {
      act01: {
        nameF: "Julie",
        nameL: "Tamiko Manning",
        chara: "Vic",
      },
      act02: {
        nameF: "Natalie",
        nameL: "Liconti",
        chara: "Flo",
      },
      act03: {
        nameF: "Leslie",
        nameL: "Baker",
        chara: "Jackie",
      },
      act04: {
        nameF: "Alexandre",
        nameL: "Lavigne",
        chara: "Daniel",
      },
    },
    desig: {
      des01: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Set",
        titlF: "Décor",
      },
      des02: {
        nameF: "Fruzsina",
        nameL: "Lànyi",
        titlE: "Costumes",
        titlF: "Costume",
      },
      des03: {
        nameF: "Tim",
        nameL: "Rodrigues",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des04: {
        nameF: "Peter",
        nameL: "Cerone",
        titlE: "Sound",
        titlF: "Son",
      },
      des05: {
        nameF: "Mariana",
        nameL: "Frandsen",
        titlE: "Video",
        titlF: "Vidéo",
      },
      des06: {
        nameF: "Emlyn",
        nameL: "vanBruinswaardt",
        titlE: "Stage Manager",
        titlF: "Régisseur",
      },
      des07: {
        nameF: "Cristina",
        nameL: "Cugliandro",
        titlE: "Assistant Director",
        titlF: "Assistant m-e-s",
      },
    },
    copro: {
      texts: {
        par01: "Thank you for the generous support of Canadian Actors' Equity Association and Playwrights' Workshop Montreal. Talisman Theatre engages, under the terms of the Indie Policy, professional artists who are members of the Canadian Actor's Equity Association.",
      },
    },
    photo: {
      pho01: {
        nameF: "",
        nameL: "",
        image: "2017-Vic-and-Flo-8558.jpg",
      },
      pho02: {
        nameF: "",
        nameL: "",
        image: "2017-Vic-and-Flo-8581.jpg",
      },
      pho03: {
        nameF: "",
        nameL: "",
        image: "2017-Vic-and-Flo-8668.jpg",
      },
      pho04: {
        nameF: "",
        nameL: "",
        image: "2017-Vic-and-Flo-9049.jpg",
      },
      pho05: {
        nameF: "",
        nameL: "",
        image: "2017-Vic-and-Flo-9134.jpg",
      },
      pho06: {
        nameF: "",
        nameL: "",
        image: "2017-Vic-and-Flo-9134.jpg",
      },
      pho07: {
        nameF: "",
        nameL: "",
        image: "2017-Vic-and-Flo-9149.jpg",
      },
      pho08: {
        nameF: "",
        nameL: "",
        image: "2017-Vic-and-Flo-9392.jpg",
      },
      pho09: {
        nameF: "",
        nameL: "",
        image: "2017-Vic-and-Flo-9404.jpg",
      },
    },
    criti: {
      cri01: {
        nameF: "Sylvain",
        nameL: "Richard",
        title: "Culture Plus",
        words: "I found the play to be enjoyable, well acted and well written. It engaged my attention throughout. The characters showed depth and were superbly interpreted...",
        dates: "November 26, 2017",
        links: "2017-vf-SMR.pdf",
      },
      cri02: {
        nameF: "andrine",
        nameL: "Vieira",
        title: "Le Culte",
        words: "The four actors managed to bring intense emotions through their superb talent, not to mention the sound effects and visuals that brought their share of success to the play.",
        dates: "November 26, 2017",
        links: "2017-vf-LC.pdf",
      },
      cri03: {
        nameF: "Nathalie",
        nameL: "Lessard",
        title: "Pieuvre.ca",
        words: "This intimate thriller, original and sylvan, leaves a strong impression and contains sublime parts of shadows and lights to discover.",
        dates: "November 27, 2017",
        links: "2017-vf-PCA.pdf",
      },
      cri04: {
        nameF: "Byron",
        nameL: "Toben",
        title: "Wesmountmag.ca",
        words: "I must admit that the leisurely artistic pace of the first third of <em>Vic & Flo</em> seemed somewhat achingly slow, but it was paced beautifully to match the unfolding developments.",
        dates: "November 27, 2017",
        links: "2017-vf-WM.pdf",
      },
      cri05: {
        nameF: "Jonathan",
        nameL: "Duchesne",
        title: "Nevro'sArts",
        words: "In his adapatation, Michael Mackenzie has not only translated the text well, but also captured the ambivalent atmosphere that is so characteristic of Côté's work.",
        dates: "November 26, 2017",
        links: "2017-vf-NA.pdf",
      },
      cri06: {
        nameF: "Élie",
        nameL: "Castiel",
        title: "SÉQUENCES",
        words: "Michael MacKenzie comes out the winner in this enterprise, with his discipline, sense of dark humor, and the skill to succeed...",
        dates: "November 24, 2017",
        links: "2017-vf-RS.pdf",
      },
    },
    logos: {
      log01: {
        image: "CCFA_RGB_colour_e.jpg",
      },
      log02: {
        image: "Calq_cyan.jpg",
      },
      log03: {
        image: "logo-cole.jpg",
      },
      log04: {
        image: "CMYK_Logo_CAM+Montreal.jpg",
      },
    },
  },
  {
    pyear: "2016",
    dateE: "October 13-29, 2016",
    dateF: "13-29 octobre, 2016",
    place: "La Chapelle Theatre",
    title: "Yukonstyle",
    image: "poster-2016_en_yukonstyle.jpg",
    bannE: "banner-2016_en_yukonstyle.jpg",
    bannF: "banner-2016_fr_yukonstyle.jpg",
    synop: {
      texts: {
        par01: "At the heart of <em>Yukonstyle</em> we find youth stripped of everything they know, flinging themselves into a distant elsewhere to try to numb their pain, or try to find a space large enough to contain the dizziness they feel, to match the Yukon landscape, whose motto is 'Larger than life'. Sarah, the author, states: '... I wanted raw, brave, avid and dazzling characters. Four solitudes that come together, console and love each other despite themselves, the confluence of life and death in the middle of a winter that never ends.'",
        par02: "I want to inflate myself with landscape and explode like a birthday balloon; become a rain of confetti that would snow, peaceful, in the sharp Yukon night.'--Kate",
        par03: "And in this imaginary Yukon, through the shock of their improbable encounters, emerges a little humanity and a hint of tenderness, which provide them with the resilience to get through the cold and continue their quest. A place where primal dialogues meet poetic monologues, and narrations reveal the fragments of humanity in an arid and unforgiving environment.",
        par04: "Talisman is excited to bring the framework of Sarah's francophone lyricism, her magic-realist, tragi-comic perspective to the English stage. This is an important play that embraces thorny topics.",
      },
    },
    auths: {
      aut01: {
        nameF: "Sarah",
        nameL: "Berthiaume",
      },
    },
    trans: {
      tra01: {
        nameF: "Nadine",
        nameL: "Desrochers",
      },
    },
    drama: {
      dra01: {
        nameF: "",
        nameL: "",
      },
    },
    direc: {
      dir01: {
        nameF: "Geneviève L.",
        nameL: "Blais",
      },
    },
    chore: {
      cho01: {
        nameF: "",
        nameL: "",
      },
    },
    actor: {
      act01: {
        nameF: "Justin",
        nameL: "Many Fingers",
        chara: "Garin",
      },
      act02: {
        nameF: "Jasmine",
        nameL: "Chen",
        chara: "Yuko",
      },
      act03: {
        nameF: "Julia",
        nameL: "Borsellino",
        chara: "Kate",
      },
      act04: {
        nameF: "Chip",
        nameL: "Chuipka",
        chara: "Pops",
      },
    },
    desig: {
      des01: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Set",
        titlF: "Décor",
      },
      des02: {
        nameF: "Fruzsina",
        nameL: "Lànyi",
        titlE: "Costumes",
        titlF: "Costume",
      },
      des03: {
        nameF: "David",
        nameL: "Perreault Ninacs",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des04: {
        nameF: "Navet",
        nameL: "Confit",
        titlE: "Sound",
        titlF: "Son",
      },
      des05: {
        nameF: "Claude",
        nameL: "Lemelin",
        titlE: "Stage Manager",
        titlF: "Régisseur",
      },
    },
    copro: {
      texts: {
        par01: "Thank you for the generous support of Canadian Actors' Equity Association and Playwrights' Workshop Montreal. Talisman Theatre engages, under the terms of the Indie Policy, professional artists who are members of the Canadian Actor's Equity Association. ",
      },
    },
    photo: {
      pho01: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-0141.jpg",
      },
      pho02: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-0179.jpg",
      },
      pho03: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-0194.jpg",
      },
      pho04: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-0214.jpg",
      },
      pho05: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-0224.jpg",
      },
      pho06: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-0265.jpg",
      },
      pho07: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-0352.jpg",
      },
      pho08: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-0359.jpg",
      },
      pho09: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-0384.jpg",
      },
      pho10: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-0405.jpg",
      },
      pho11: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-8900.jpg",
      },
      pho12: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-8942.jpg",
      },
      pho13: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-8955.jpg",
      },
      pho14: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-8988.jpg",
      },
      pho15: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-8999.jpg",
      },
      pho16: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-9004.jpg",
      },
      pho17: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-9066.jpg",
      },
      pho18: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-9111.jpg",
      },
      pho19: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-9184.jpg",
      },
      pho20: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-9203.jpg",
      },
      pho21: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-9245.jpg",
      },
      pho22: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-9250.jpg",
      },
      pho23: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-9260.jpg",
      },
      pho24: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-9423.jpg",
      },
      pho25: {
        nameF: "",
        nameL: "",
        image: "2016-Yukonstyle-9868.jpg",
      },
    },
    criti: {
      cri01: {
        nameF: "Jim",
        nameL: "Burke",
        title: "The Montreal Gazette",
        words: "...on the whole this is a striking, often richly imaginative play, skilfully directed by Geneviève L. Blais.",
        dates: "October 14, 2016",
        links: "2016-ys-gazette.pdf",
      },
      cri02: {
        nameF: "Angela",
        nameL: "Potvin",
        title: "Mtl Rampage",
        words: "I cannot get enough of this type of theatre. The production is slick but not distracting, the actors are as solid as a brick house and the gaze is unflinching on the realities that they are dealing with. It is gutsy, challenging yet accessible, and deeply unforgiving work. I felt vulnerable at the end of this. It deserves to be widely seen. Talisman Theatre, I salute you.",
        dates: "October 15, 2016",
        links: "2016-ys-rampage.pdf",
      },
      cri03: {
        nameF: "Byron",
        nameL: "Toben",
        title: "Westmount mag",
        words: "This play has drawn praise in Toronto and Europe. I would recommend it...",
        dates: "October 14, 2016",
        links: "2016-ys-westmount.pdf",
      },
      cri04: {
        nameF: "Camila",
        nameL: "Fitzgibbon",
        title: "Theatre Hub",
        words: "...Maintaining its mandate of adapting contemporary French Quebec plays to the English stage (credits to translator Nadine Desrochers for preserving the lyrical quality of Berthiaume’s script), Talisman Theatre is to be commended here for providing a voice for the people of the First Nations and bringing their issues front and centre. <em>Yukonstyle</em> is overeager to simultaneously emote, entertain, and educate... <em>Yukonstyle</em> succeeds in embracing and exploring relevant issues of identity, diversity, and multiculturalism. More vitally, it illustrates the impact of the national tragedy of Canada’s murdered aboriginal women, blowing the whistle on a system that has failed to protect them and on a onlooking society that has left the native community to fend for itself in the most hostile and unforgiving of conditions. Above all, it ignites essential conversations on our search for roots, how we cope with loss, and our glaring need for human connection and intimacy.",
        dates: "October 19, 2016",
        links: "2016-ys-hub.pdf",
      },
      cri05: {
        nameF: "Sébastien",
        nameL: "Bouthiller",
        title: "MATTV",
        words: "...Larger than life , la devise yukonaise acquiert un sens inédit dans cette pièce de Sarah Berthiaume, qui s’est inspirée de personnages réels qu’elle a croisés lors d’un séjour dans ce territoire où le mercure chute à moins 45 ˚Celsius. À travers des dialogues crus et des monologues poétiques, l’auteure exprime la résilience de personnages sensibles qui se protègent d’abord d’eux-mêmes dans cet environnement aride. ...Lors de sa création en 2013, <em>Yukonstyle</em> a été présentée simultanément à Montréal, au Théâtre d’Aujourd’hui et à Paris. Une tournée a ensuite mené la pièce sur les scènes de Toronto, Innsbruck, Bruxelles et Heidelberg. Cela confirme le talent de l’auteure pour l’écriture de pièces aux thèmes universels...",
        dates: "October 14, 2016",
        links: "2016-ys-MATTV.pdf",
      },
      cri06: {
        nameF: "Joshua",
        nameL: "De Costa",
        title: "Concordia Link",
        words: "Berthiaums's <em>Yukonstyle</em> provokes audiences by giving them a glimpse of what the harrowing final moments of Pickton's vicims might have looked like through Garin's narrative. [...] Some members of the audience cried, as the fictional scence came close to reality.",
        dates: "October 18, 2016",
        links: "2016-ys-link.pdf",
      },
      cri07: {
        nameF: "Jean",
        nameL: "Hostache",
        title: "Un fauteuil pour l’orchestre",
        words: "<em>Yukonstyle</em> c’est faire la découverte d’un texte puissant signé Sarah Berthiaume, dramaturge québécoise, ici magnifiquement traduit en anglais par Nadine Desrochers. Un texte glaçant, au verbe qui s’articule dans la buée d’un hiver éternel, mais qui envers et contre tout réchauffe les esprits... Portée par quatre acteurs brillants, la mise en scène de <em>Yukonstyle</em> se confond avec la photographie d’un film.... cette adaptation anglaise, parfaitement limpide pour les non-anglophones, nous dérange, nous amuse tant qu’elle nous fait peur. La compagnie du Talisman Theater a parfaitement trouvé le dosage entre l’humour, la noirceur, et le militantisme théâtral, signant une mise en scène forte et sensible",
        dates: "October 21, 2016",
        links: "2016-ys-fauteuil.pdf",
      },
      cri08: {
        nameF: "Gilles",
        nameL: "Lamontagne",
        title: "Sorstu.ca",
        words: "Évoluant entre trois structures de maisons mobiles pour tout décor (conçu habilement par Lyne Paquette), l’histoire de <em>Yukonstyle</em> est aussi banale que criante de vérité....Quatre comédiens d’horizons différents (Toronto, Collège Dawson à Montréal, Nord de l’Ontario, Université de la Saskatchewan, Réserve Blackfoot du Sud de l’Alberta) défendent le texte avec beaucoup de conviction. On voit tout de suite qu’ils ont été bien dirigés par Geneviève L. Blais... <em>Yukonstyle</em> est mue par une grande force de survie devant les revers de vies sacrifiées, une force admirablement transposée sur cette scène de La Chapelle.",
        dates: "October 20, 2016",
        links: "2016-ys-sors-tu.pdf",
      },
      cri09: {
        nameF: "Pierre-Alexandre",
        nameL: "Buisson",
        title: "Bible Urbaine",
        words: "Certains mécanismes dans la mise en scène de Geneviève L. Blais sont plutôt inventifs... le temps file sans que le spectateur s’ennuie. La traduction de Nadine Desrochers du texte original de Sarah Berthiaume est très fluide, et la mission du Talisman Theatre est noble: faire découvrir la dramaturgie francophone à un public anglophone. Les quatre comédiens offrent des interprétations dans le ton, mais on n’a d’yeux que pour Justin Many Fingers, avec sa combinaison de stature et d’intensité, qui semble plus grand que nature. Rarement a-t-on vu un comédien transmettre aussi justement un immense tourment interne.",
        dates: "October 23, 2016",
        links: "2016-ys-bible.pdf",
      },
    },
    logos: {
      log01: {
        image: "CCFA_RGB_colour_e.jpg",
      },
      log02: {
        image: "Calq_cyan.jpg",
      },
      log03: {
        image: "logo-cole.jpg",
      },
      log04: {
        image: "CMYK_Logo_CAM+Montreal.jpg",
      },
    },
  },
  {
    pyear: "2016",
    dateE: "May 18-21, 2016",
    dateF: "18-21 mai, 2016",
    place: "Montréal, arts interculturels",
    title: "Me and You",
    image: "poster-2016_en_meyou.jpg",
    bannE: "banner-2016_en_meyou.jpg",
    bannF: "banner-2016_fr_meyou.jpg",
    synop: {
      texts: {
        par01: "A young actress recalls her childhood memories and her arrival in Quebec. As the story unfolds, she recounts her meeting with Julie Sirois, her best friend—a quebecois who dies in an accident. Denying this reality, Talia will interrupt the story and change the course of her destiny by giving her role to Julie. But she soon discovers that sharing her life with others has risks, especially when it comes to uniting two cultures that everything dissociates. Autofiction where reality and freedom are scrambled, <a>Me and You</em> portrays a friendship that defies conventions and looks at the identity of the immigrant, posing the question : Do we acknowledge the Other for what he gives us, or for what takes from us?",
        par02: "Hallmona asks: 'Who am I in the eyes of others ?'",
        par03: "The story deals with the integration of immigrants in Quebec. The last day in the homeland. The arrival in Quebec. The dream. Friendship. Wanting to become what we are not...",
        par04: "<em>Me and You</em> is written by Pascal Brullemans and Talia Hallmona who were awarded the Louise-LaHaye prize for <em>Moi et l’Autre</em>. This award recognizes the excellence of a play for young audience.",
      },
    },
    auths: {
      aut01: {
        nameF: "Pascal",
        nameL: "Brullemans",
      },
      aut02: {
        nameF: "Talia",
        nameL: "Hallmona",
      },
    },
    trans: {
      tra01: {
        nameF: "Alison",
        nameL: "Bowie",
      },
    },
    drama: {
      dra01: {
        nameF: "Ülfet",
        nameL: "Sevdi",
      },
      dra02: {
        nameF: "Louis Patrick",
        nameL: "Leroux",
      },
    },
    direc: {
      dir01: {
        nameF: "Arianna",
        nameL: "Bardesono",
      },
    },
    chore: {
      cho01: {
        nameF: "",
        nameL: "",
      },
    },
    actor: {
      act01: {
        nameF: "Mireille",
        nameL: "Tawfik",
        chara: "Talia",
      },
      act02: {
        nameF: "Kathleen",
        nameL: "Stavert",
        chara: "Julie",
      },
    },
    desig: {
      des01: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Set",
        titlF: "Décor",
      },
      des02: {
        nameF: "Fruzsina",
        nameL: "Lànyi",
        titlE: "Costumes",
        titlF: "Costume",
      },
      des03: {
        nameF: "David Alexandre",
        nameL: "Chabot",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des04: {
        nameF: "Jesse",
        nameL: "Ash",
        titlE: "Sound",
        titlF: "Son",
      },
      des05: {
        nameF: "Jesse",
        nameL: "Ash",
        titlE: "Composer",
        titlF: "Compositeur",
      },
      des06: {
        nameF: "Emlyn",
        nameL: "Vanbruinswaardt",
        titlE: "Stage Manager",
        titlF: "Régisseur",
      },
    },
    copro: {
      texts: {
        par01: "Thank you for the generous support of Canadian Actors' Equity Association and Playwrights' Workshop Montreal. Talisman Theatre engages, under the terms of the Indie Policy, professional artists who are members of the Canadian Actor's Equity Association.",
      },
    },
    photo: {
      pho01: {
        nameF: "",
        nameL: "",
        image: "2016-Me&You-7683.jpg",
      },
      pho02: {
        nameF: "",
        nameL: "",
        image: "2016-Me&You-7961.jpg",
      },
      pho03: {
        nameF: "",
        nameL: "",
        image: "2016-Me&You-8050.jpg",
      },
      pho04: {
        nameF: "",
        nameL: "",
        image: "2016-Me&You-8099.jpg",
      },
      pho05: {
        nameF: "",
        nameL: "",
        image: "2016-Me&You-8127.jpg",
      },
      pho06: {
        nameF: "",
        nameL: "",
        image: "2016-Me&You-8174.jpg",
      },
      pho07: {
        nameF: "",
        nameL: "",
        image: "2016-Me&You-8436.jpg",
      },
      pho08: {
        nameF: "",
        nameL: "",
        image: "2016-Me&You-8518.jpg",
      },
      pho09: {
        nameF: "",
        nameL: "",
        image: "2016-Me&You.jpg",
      },
    },
    criti: {
      cri01: {
        nameF: "Sylvain",
        nameL: "Richard",
        title: "Culture plus",
        words: "The translation works well and shows that the questions of integration and identity affect everyone universally. We are all in one way or another faced with integrating into society and discovering who we really are. The chemistry between the two principle characters was emotionally engaging. The performance and dramaturgy was A1.",
        dates: "May 20, 2016",
        links: "2016-meandyou-cultureplus.pdf",
      },
      cri02: {
        nameF: "Byron",
        nameL: "Toben",
        title: "Westmountmag.ca",
        words: "The set... was essentially bare, with many light bulbs hanging or standing on steps, lending a vaguely surreal quality to the whole... The whole is ably directed by NTS grad and teacher Arianna Bardesono, herself from Italy years ago.",
        dates: "May 20, 2016",
        links: "2016-meandyou-westmountmag.pdf",
      },
      cri03: {
        nameF: "Alice",
        nameL: "Caron",
        title: "Patwhite.Com",
        words: "This sensitive interpretation of uprooting is amplified by the staging, as sleek as poetic.",
        dates: "May 21, 2016",
        links: "2016-meandyou-patwhite.pdf",
      },
      cri04: {
        nameF: "Camila",
        nameL: "Fitzgibbon",
        title: "Theatre HUB",
        words: "<em>Me and You</em> pulls at the heartstrings in playing themes of belonging, acceptance, and connection.",
        dates: "May 20, 2016",
        links: "2016-meandyou-MontrealHUB.pdf",
      },
      cri05: {
        nameF: "Edit",
        nameL: "Jakab",
        title: "cultMontreal",
        words: "Sincere acting, original and minimalist usage of space, props and costumes, the two young protagonists fills the theatre with realist magic while urging us to ponder the deeper meanings of our sense of self and that of others.",
        dates: "May 20, 2016",
        links: "2016-meandyou-cultMontreal.pdf",
      },
      cri06: {
        nameF: "Charlotte",
        nameL: "Mercille",
        title: "Bible Urbaine",
        words: "The minimalist staging by Arianna Bardesono takes us to the heart of a story of initiation or even a autofiction deeply sensitive and the reality of a growing number of 'new' Quebec.",
        dates: "May 21, 2016",
        links: "2016-meandyou-bible.pdf",
      },
      cri07: {
        nameF: "Anna",
        nameL: "Fuerstenberg",
        title: "Mtl Rampage",
        words: "The actors were very good and Miriam Katrib managed to convey the confusion and pathos of the immigrant first generation with tremendous energy. Kathleen Stavert gave and outstanding performance as the Quebecois girl who befriends Talia, and tries to teach her in one of the most comic scenes how to attract French boys.",
        dates: "May 20, 2016",
        links: "2016-meandyou-rampage.pdf",
      },
    },
    logos: {
      log01: {
        image: "CCFA_RGB_colour_e.jpg",
      },
      log02: {
        image: "Calq_cyan.jpg",
      },
      log03: {
        image: "logo-cole.jpg",
      },
      log04: {
        image: "CMYK_Logo_CAM+Montreal.jpg",
      },
    },
  },
  {
    pyear: "2015",
    dateE: "October 7-17, 2015",
    dateF: "7-17 octobre, 2015",
    place: "The Centaur Theatre",
    title: "Province",
    image: "poster-2015_province.jpg",
    bannE: "banner-2015_en_province1.jpg",
    bannF: "banner-2015_fr_province1.jpg",
    synop: {
      texts: {
        par01: "Set in the deep forest of Quebec, this darkly comic play is part of a rich tradition of dystopian art, exploring humanity’s indifference to environmental destruction and our commitment to individualism at any cost. A poetic, whimsical fable, steeped in organic rural textures, <em>Province</em> revolves around characters who must face off against mutant animals. Despite the ecological malaise, the people of Province continue their pre-apocalypse lives with as much gusto as before, playing Wii, making home videos and obsessing over their looks.",
        par02: "'The forest's advance, the animals' unnatural frenzy, that horrific vision. I'm starting to connect the dots and the big picture is dreadful.'--Hide",
        par03: "'Among the murmur of the thistle and nettle, you can hear the dribble of drool over fangs. Eyes closed, you can almost see the manufactured and mutant shapes of the unseen.'--Buddy",
        par04: "<em>Province</em> explores humanity’s indifference to environmental destruction and its commitment to individualism at any cost. It asks whether our adaptability—our greatest human strength—may actually be what is holding us back from finding common sense solutions to our generation’s most pressing problem. <em>Province</em> fits perfectly into the aesthetic niche created by our past productions.",
        par05: "<em>Province</em> is written by Mathieu Gosselin, a Quebecois playwright/actor in his early thirties. <em>Province</em>, was first staged by Théâtre de la Banquette Arrière at La Licorne in May 2012.",
        par06: "This play has never been produced in English!!! Talisman's 2015 production will be Canada's English-language premiere of this remarkable work. It's getting warmer; you can smell the fear in the air. Nature is running amok. Climate change or a much darker, transfiguring force.",
        par07: "<em>Province</em> takes the audience on a wild ride, full of potent questions and uneasy answers.",
      },
    },
    auths: {
      aut01: {
        nameF: "Mathieu",
        nameL: "Gosselin",
      },
    },
    trans: {
      tra01: {
        nameF: "Nadine",
        nameL: "Desrochers",
      },
    },
    drama: {
      dra01: {
        nameF: "",
        nameL: "",
      },
    },
    direc: {
      dir01: {
        nameF: "Stacey",
        nameL: "Christodoulou",
      },
    },
    chore: {
      cho01: {
        nameF: "",
        nameL: "",
      },
    },
    actor: {
      act01: {
        nameF: "Éloi",
        nameL: "ArchamBaudoin",
        chara: "",
      },
      act02: {
        nameF: "Stefanie",
        nameL: "Buxton",
        chara: "",
      },
      act03: {
        nameF: "Davide",
        nameL: "Chiazzese",
        chara: "",
      },
      act04: {
        nameF: "Zach",
        nameL: "Fraser",
        chara: "",
      },
      act05: {
        nameF: "Mike",
        nameL: "Hughes",
        chara: "",
      },
      act06: {
        nameF: "Sabrina",
        nameL: "Reeves",
        chara: "",
      },
      act07: {
        nameF: "Jennifer",
        nameL: "Roberts",
        chara: "",
      },
      act08: {
        nameF: "France",
        nameL: "Rolland",
        chara: "",
      },
      act09: {
        nameF: "Natalie",
        nameL: "Tannous",
        chara: "",
      },
      act10: {
        nameF: "Tristan",
        nameL: "D.Lalla",
        chara: "(voice)",
      },
    },
    desig: {
      des01: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Set",
        titlF: "Décor",
      },
      des02: {
        nameF: "Fruzsina",
        nameL: "Lànyi",
        titlE: "Costumes",
        titlF: "Costume",
      },
      des03: {
        nameF: "David",
        nameL: "Perreault Ninacs",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des04: {
        nameF: "Jesse",
        nameL: "Ash",
        titlE: "Sound",
        titlF: "Son",
      },
      des05: {
        nameF: "Jesse",
        nameL: "Ash",
        titlE: "Composer",
        titlF: "Compositeur",
      },
      des06: {
        nameF: "Barbara",
        nameL: "Zsigovics",
        titlE: "Stage Manager",
        titlF: "Régisseur",
      },
    },
    copro: {
      texts: {
        par01: "Thank you for the generous support of Canadian Actors' Equity Association and Playwrights' Workshop Montreal. Talisman Theatre engages, under the terms of the Indie Policy, professional artists who are members of the Canadian Actor's Equity Association.",
      },
    },
    photo: {
      pho01: {
        nameF: "",
        nameL: "",
        image: "2015-Province-5605.jpg",
      },
      pho02: {
        nameF: "",
        nameL: "",
        image: "2015-Province-5684.jpg",
      },
      pho03: {
        nameF: "",
        nameL: "",
        image: "2015-Province-5784.jpg",
      },
      pho04: {
        nameF: "",
        nameL: "",
        image: "2015-Province-6006.jpg",
      },
      pho05: {
        nameF: "",
        nameL: "",
        image: "2015-Province-6190.jpg",
      },
      pho06: {
        nameF: "",
        nameL: "",
        image: "2015-Province-6237.jpg",
      },
      pho07: {
        nameF: "",
        nameL: "",
        image: "2015-Province-6259.jpg",
      },
      pho08: {
        nameF: "",
        nameL: "",
        image: "2015-Province-6305.jpg",
      },
      pho09: {
        nameF: "",
        nameL: "",
        image: "2015-Province-6320.jpg",
      },
      pho10: {
        nameF: "",
        nameL: "",
        image: "2015-Province-6381.jpg",
      },
      pho11: {
        nameF: "",
        nameL: "",
        image: "2015-Province-6449.jpg",
      },
      pho12: {
        nameF: "",
        nameL: "",
        image: "2015-Province-6495.jpg",
      },
      pho13: {
        nameF: "",
        nameL: "",
        image: "2015-Province-6616.jpg",
      },
      pho14: {
        nameF: "",
        nameL: "",
        image: "2015-Province-6706.jpg",
      },
    },
    criti: {
      cri01: {
        nameF: "Julia",
        nameL: "Bryant",
        title: "The Concordian",
        words: "If you’ve never been lost in the depths of the wild forests of Quebec, then <em>Province</em>--a new play staged at the Centaur Theatre--would have been the perfect way to feel like you were. ... It was a hauntingly beautiful piece. It was eerily unpredictable and somewhat confusing--just what you would expect from a dystopian drama which features mutant animals.",
        dates: "October 20, 2015",
        links: "2015-province-Concordian.pdf",
      },
      cri02: {
        nameF: "Arianna",
        nameL: "Bardesono",
        title: "Student Group Leader",
        words: "Un énorme bravo, tout d'abord à Stacey, chaque élément de la scène est qualitativement et conceptuellement très bien articulé. Les personnages font un lien efficace entre le monde surréel de la pièce et un ancrage nécessaire avec notre société fragmenté et narcissique. Si j'ajoute le temps que vous tous avez eu pour monter cette pièce j'en suis vraiment impressionnée. La pièce habite un univers très loin du mien, mais hier soir j'ai apprécié cet déplacement ailleurs.",
        dates: "October 20, 2015",
        links: "",
      },
      cri03: {
        nameF: "",
        nameL: "",
        title: "Mountain Lake PBS",
        words: "<em>Province</em>'s tragic ending helps examine the tremendous resilience of human beings in the face of adversity and offer spectators the chance to reflect upon and contemplate the impact we humans currently have on nature and the uncertainty of our own future with a visionary theatrical approach.",
        dates: "October 20, 2015",
        links: "2015-province-mountain-01.pdf",
      },
      cri04: {
        nameF: "Byron",
        nameL: "Toben",
        title: "Westmountmag.ca",
        words: "The critters and revengeful forces are never seen but heard, ethereally voiced off stage by Tristan D. Lalla. Lots of talented local actors in this one.",
        dates: "October 20, 2015",
        links: "2015-province-westmount-01.pdf",
      },
      cri05: {
        nameF: "nic and emily",
        nameL: "",
        title: "Re:Stage Reviews",
        words: "...What the show excels in is the atmosphere of it all ...this sense of impending doom.",
        dates: "October 20, 2015",
        links: "",
      },
      cri06: {
        nameF: "Lisa",
        nameL: "Trotto",
        title: "Student Group Leader",
        words: "I must say this was one of the best plays I have ever seen at The Centaur. My students and I couldn't stop laughing throughout the play (especially when the 'plastic' couple made their appearance). ...It was absolutely excellent and a clear message was delivered through the play. ...Overall, well written, acted and well worth a standing ovation.",
        dates: "October 20, 2015",
        links: "",
      },
      cri07: {
        nameF: "Jim",
        nameL: "Burke",
        title: "The Gazette",
        words: "Like a Jacobean revenge tragedy of the future, Mathieu Gosselin’s end-of-the-world play <em>Province</em> climaxes with the stage littered in corpses. ...There’s something Jacobean also in Gosselin’s bold use of poetic language, translated here by Nadine Desrochers, and in its wild lurches from the deadly serious to the grotesquely comic.",
        dates: "October 9, 2015",
        links: "2015-province-Gazette-02.pdf",
      },
      cri08: {
        nameF: "Charlotte",
        nameL: "Mercille",
        title: "Bible Urbaine",
        words: "Sur fond de dystopie écologique, la large distribution livre une performance impeccable qui en dit long sur la nature profonde de l’être humain et son pouvoir d’autodestruction.",
        dates: "October 20, 2015",
        links: "2015-province-BibleU-01.pdf",
      },
    },
    logos: {
      log01: {
        image: "CCFA_RGB_colour_e.jpg",
      },
      log02: {
        image: "Calq_cyan.jpg",
      },
      log03: {
        image: "logo-cole.jpg",
      },
      log04: {
        image: "CMYK_Logo_CAM+Montreal.jpg",
      },
    },
  },
  {
    pyear: "2015",
    dateE: "February 3-7, 2015",
    dateF: "3-7 février, 2015",
    place: "Segal Center Studio",
    title: "The Medea Effect (remount)",
    image: "poster-2015_medea.jpg",
    bannE: "banner-2015_en_medea2.jpg",
    bannF: "banner-2015_fr_medea2.jpg",
    synop: {
      texts: {
        par01: "Ada is a mother who has forgotten her child during an emotional trauma. Ugo's childhood fear has come to pass, his mother has forgotten him. <em>The Medea Effect</em> is an emotional roller-coaster ride launching the audience into abstract heights and plunging them into emotional depths. It is also an emotional tug-of-war between two isolated minds over the same subject: the mother.",
        par02: "Description: Written by Quebecois playwright Suzie Bastien, translated for Talisman Theatre by Nadine Desrochers, and directed by Emma Tibaldo, <em>The Medea Effect</em> has never been produced in English in Montreal! Opening February 3 to 7,2015 at Segal Center Studio, <em>The Medea Effect</em> will star Jennifer Morehouse as Ada and James Loye as Ugo. <em>The Medea Effect</em> is like quick-sand--the unwitting audience member is lured into its emotional quagmire by the familiar surface of simple speech and common empathy. Simple speech is revealed to be a complex system of references with no inside nor outside, no beginning nor end. Once trapped, the audience is drawn inexorably into a region of profound philosophical inquiry.",
      },
    },
    auths: {
      aut01: {
        nameF: "Suzie",
        nameL: "Bastien",
      },
    },
    trans: {
      tra01: {
        nameF: "Nadine",
        nameL: "Desrochers",
      },
    },
    drama: {
      dra01: {
        nameF: "",
        nameL: "",
      },
    },
    direc: {
      dir01: {
        nameF: "Emma",
        nameL: "Tibaldo",
      },
    },
    chore: {
      cho01: {
        nameF: "",
        nameL: "",
      },
    },
    actor: {
      act01: {
        nameF: "James",
        nameL: "Loye",
        chara: "Ugo",
      },
      act02: {
        nameF: "Jennifer",
        nameL: "Morehouse",
        chara: "Ada",
      },
    },
    desig: {
      des01: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Set",
        titlF: "Décor",
      },
      des02: {
        nameF: "Fruzsina",
        nameL: "Lànyi",
        titlE: "Costumes",
        titlF: "Costume",
      },
      des03: {
        nameF: "Angeline",
        nameL: "St-Amour",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des04: {
        nameF: "Matthew",
        nameL: "Waddell",
        titlE: "Sound",
        titlF: "Son",
      },
      des05: {
        nameF: "Michael",
        nameL: "Leon",
        titlE: "Composer",
        titlF: "Compositeur",
      },
      des06: {
        nameF: "Rasili",
        nameL: "Botz",
        titlE: "Movement",
        titlF: "Mouvement",
      },
      des07: {
        nameF: "Jean",
        nameL: "Ranger",
        titlE: "Video",
        titlF: "Vidéo",
      },
    },
    copro: {
      texts: {
        par01: "Thank you for the generous support of Canadian Actors' Equity Association and Playwrights' Workshop Montreal. Talisman Theatre engages, under the terms of the Guest Artist Policy, professional artists who are members of the Canadian Actor's Equity Association.",
      },
    },
    photo: {
      pho01: {
        nameF: "",
        nameL: "",
        image: "2012-tme1_12.jpg",
      },
      pho02: {
        nameF: "",
        nameL: "",
        image: "2012-tme1_5.jpg",
      },
      pho03: {
        nameF: "",
        nameL: "",
        image: "2012-tme_DSCF3726.jpg",
      },
    },
    criti: {
      cri01: {
        nameF: "Synden",
        nameL: "Hope-Johnston",
        title: "MTLBlog",
        words: "<em>The Medea Effect</em> slowly builds on its dark tones and moods until finally brought to the bone-chilling climax, effectively drawing the audience into the very depths of the characters' inevitable unhinging. Everyone, from actor to viewer alike, is left marked, as though they've just barely managed to come out the other side.",
        dates: "February 5, 2015",
        links: "2015-medea-01.pdf",
      },
      cri02: {
        nameF: "Élie",
        nameL: "Castiel",
        title: "SÉQUENCES",
        words: "Si l’on en juge par le poids éloquent des mots, la psychologie incontestablement humaniste des réparties, la teneur vigoureuse du propos et la force des échanges entre deux êtres à la recherche d’une âme, la traduction de <em>L’Effet Médée</em>, de Suzie Bastien, par Nadine Desrochers, est un tour de force admirable.",
        dates: "February 4, 2015",
        links: "2015-medea-02.pdf",
      },
      cri03: {
        nameF: "Jim",
        nameL: "Burke",
        title: "Revue Roverarts",
        words: "If the plot of <em>The Medea Effect</em> sounds familiar--mysterious actress turns up to audition for formidable female role, confounds male director's intitial scepticism... Ives also brought classical myth crashing into the quotidian world with spectaular results, conjuring up an almighty Aphrodite to crush male presumption under her stiletto heel. (For the record, Bastien’s play predates Ives' by several years).",
        dates: "February 3, 2015",
        links: "2015-medea-03.pdf",
      },
    },
    logos: {
      log01: {
        image: "CCFA_RGB_colour_e.jpg",
      },
      log02: {
        image: "Calq_cyan.jpg",
      },
      log03: {
        image: "logo-cole.jpg",
      },
      log04: {
        image: "CMYK_Logo_CAM+Montreal.jpg",
      },
    },
  },
  {
    pyear: "2014",
    dateE: "October 14-25, 2014",
    dateF: "14-25 octobre, 2014",
    place: "La Chapelle Theatre",
    title: "Billy (the days of howling)",
    image: "poster-2014_billy.jpg",
    bannE: "banner-2014_en_billy.jpg",
    bannF: "banner-2014_fr_billy.jpg",
    synop: {
      texts: {
        par01: "With <em>Billy (the days of howling)</em>,Fabien Cloutier releases a tsunami of anger built entirely on prejudices, stereotypes and our ignorance of others. Three characters, who view each other as lesser or wanting, are confronted by the repercussions of unforgiving judgments. <em>Billy (the days of howling)</em> makes us reflect on our vision of community and strives to uncover why we are unable to really engage with one another.",
      },
    },
    auths: {
      aut01: {
        nameF: "Fabien",
        nameL: "Cloutier",
      },
    },
    trans: {
      tra01: {
        nameF: "Nadine",
        nameL: "Desrochers",
      },
    },
    drama: {
      dra01: {
        nameF: "",
        nameL: "",
      },
    },
    direc: {
      dir01: {
        nameF: "Emma",
        nameL: "Tibaldo",
      },
    },
    chore: {
      cho01: {
        nameF: "",
        nameL: "",
      },
    },
    actor: {
      act01: {
        nameF: "Davide",
        nameL: "Chiazzese",
        chara: "",
      },
      act02: {
        nameF: "Susan",
        nameL: "Glover",
        chara: "",
      },
      act03: {
        nameF: "Nadia.",
        nameL: "Verrucci",
        chara: "",
      },
    },
    desig: {
      des01: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Set",
        titlF: "Décor",
      },
      des02: {
        nameF: "Fruzsina",
        nameL: "Lànyi",
        titlE: "Costumes & set",
        titlF: "Costume & Décor",
      },
      des03: {
        nameF: "David",
        nameL: "Perreault Ninacs",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des04: {
        nameF: "Michael",
        nameL: "Leon",
        titlE: "Sound",
        titlF: "Son",
      },
      des05: {
        nameF: "Rasili",
        nameL: "Botz",
        titlE: "Movement",
        titlF: "Mouvement",
      },
      des06: {
        nameF: "Rachel Dawn",
        nameL: "Woods",
        titlE: "Stage Manager",
        titlF: "Régisseur",
      },
      des07: {
        nameF: "Scott",
        nameL: "Drysdale",
        titlE: "Technical Director",
        titlF: "Directeur technique",
      },
      des08: {
        nameF: "Janis",
        nameL: "Kirshner",
        titlE: "Publicist",
        titlF: "Relationiste de presse",
      },
    },
    copro: {
      texts: {
        par01: "Thank you for the generous support of Canadian Actors' Equity Association and Playwrights' Workshop Montreal. Talisman Theatre engages, under the terms of the Guest Artist Policy, professional artists who are members of the Canadian Actor's Equity Association.",
      },
    },
    photo: {
      pho01: {
        nameF: "",
        nameL: "",
        image: "2014-Billy-5689.jpg",
      },
      pho02: {
        nameF: "",
        nameL: "",
        image: "2014-Billy-5863.jpg",
      },
      pho03: {
        nameF: "",
        nameL: "",
        image: "2014-Billy-5973.jpg",
      },
      pho04: {
        nameF: "",
        nameL: "",
        image: "2014-Billy-6070.jpg",
      },
      pho05: {
        nameF: "",
        nameL: "",
        image: "2014-Billy-6127.jpg",
      },
      pho06: {
        nameF: "",
        nameL: "",
        image: "2014-Billy-6380.jpg",
      },
      pho07: {
        nameF: "",
        nameL: "",
        image: "2014-Billy-6556.jpg",
      },
      pho08: {
        nameF: "",
        nameL: "",
        image: "2014-Billy-6613.jpg",
      },
    },
    criti: {
      cri01: {
        nameF: "Pat",
        nameL: "Donnelly",
        title: "The Gazette",
        words: "Cloutier's flamboyantly overwritten play sends out a shrapnel bomb's worth of witty, piercing social commentary.",
        dates: "October 17, 2014",
        links: "2014-billy-01.pdf",
      },
      cri02: {
        nameF: "Fabien",
        nameL: "Deglise",
        title: "Le Devoir",
        words: "Un <em>Billy</em> sur la barrière linguistique des préjugés.",
        dates: "October 17, 2014",
        links: "2014-billy-02.pdf",
      },
      cri03: {
        nameF: "Cléo",
        nameL: "Mathieu",
        title: "Sors-tu?",
        words: "Une oeuvre drôle, dérangeante et actuelle.",
        dates: "October 17, 2014",
        links: "2014-billy-03.pdf",
      },
      cri04: {
        nameF: "Mike",
        nameL: "Cohen",
        title: "The Suburban",
        words: "The audience loved this beautifully written and well-acted play in a quaint theatre I had never been to before.",
        dates: "October 15, 2014",
        links: "2014-billy-06.pdf",
      },
      cri05: {
        nameF: "Pat",
        nameL: "Donnelly",
        title: "The Gazette (preview)",
        words: "Billy (the days of howling) is one of those rare plays that focus on the parenting of small children. 'It's about people who rush to judgement', Cloutier said. [...] 'The idea arrived one day when I arrived at the garderie. I wtnessed an event that pushed me to judge someone else. But I was wrong'.",
        dates: "October 9, 2014",
        links: "2014-billy-04.pdf",
      },
      cri06: {
        nameF: "Alexandre",
        nameL: "Cadieux",
        title: "Le Devoir (preview)",
        words: "PROBLEM?",
        dates: "October 11, 2014",
        links: "2014-billy-05.pdf",
      },
    },
    logos: {
      log01: {
        image: "CCFA_RGB_colour_e.jpg",
      },
      log02: {
        image: "Calq_cyan.jpg",
      },
      log03: {
        image: "logo-cole.jpg",
      },
      log04: {
        image: "CMYK_Logo_CAM+Montreal.jpg",
      },
    },
  },
  {
    pyear: "2014",
    dateE: "March 6-15, 2014",
    dateF: "6-15 mars, 2014",
    place: "La Chapelle Theatre",
    title: "The Aeneid",
    image: "poster-2014_aeneid.jpg",
    bannE: "banner-2014_en_aeneid.jpg",
    bannF: "banner-2014_fr_aeneid.jpg",
    synop: {
      texts: {
        par01: "Kemeid's <em>The Aeneid</em>, based on Virgil's poem, is a perfect blend of epic legend and contemporary reality. It is rich with scenes of drama, but also moments of humour. Kemeid's version of this classic tale migrates Aeneas's search for a homeland into the modern world of middle-eastern revolution. In this new twist, we encounter tourist resorts, refugee camps, and immigration officers, but the magic of the original epic remains in a visit to Hades, and encounters with spirits.",
        par02: "'Olivier Kemeid addresses The Aeneid as the first epic telling a great 'secular' migration'--Le Devoir (Nov, 2007)",
      },
    },
    auths: {
      aut01: {
        nameF: "Olivier",
        nameL: "Kemeid",
      },
    },
    trans: {
      tra01: {
        nameF: "Judith",
        nameL: "Miller",
      },
    },
    drama: {
      dra01: {
        nameF: "",
        nameL: "",
      },
    },
    direc: {
      dir01: {
        nameF: "Zach",
        nameL: "Fraser",
      },
    },
    chore: {
      cho01: {
        nameF: "",
        nameL: "",
      },
    },
    actor: {
      act01: {
        nameF: "Marcelo",
        nameL: "Arroyo",
        chara: "Puppeteer",
      },
      act02: {
        nameF: "Deena;",
        nameL: "Aziz",
        chara: "Puppeteer",
      },
      act03: {
        nameF: "Chimwemwe",
        nameL: "Miller",
        chara: "Puppeteer",
      },
    },
    desig: {
      des01: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Set",
        titlF: "Décor",
      },
      des02: {
        nameF: "Fruzsina",
        nameL: "Lànyi",
        titlE: "Costumes & set",
        titlF: "Costume & Décor",
      },
      des03: {
        nameF: "David",
        nameL: "Perreault Ninacs",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des04: {
        nameF: "Nikita",
        nameL: "U",
        titlE: "Sound",
        titlF: "Son",
      },
      des05: {
        nameF: "Crystal",
        nameL: "Laffoley",
        titlE: "Stage Manager",
        titlF: "Régisseur",
      },
      des06: {
        nameF: "Anne Sara",
        nameL: "Gendro",
        titlE: "Technical Director",
        titlF: "Directeur technique",
      },
      des07: {
        nameF: "Janis",
        nameL: "Kirshner",
        titlE: "Publicist",
        titlF: "Relationiste de presse",
      },
    },
    copro: {
      texts: {
        par01: "Thank you for the generous support of Canadian Actors' Equity Association and Playwrights' Workshop Montreal. Talisman Theatre engages ,under the terms of the Guest Artist Policy, professional artists who are members of the Canadian Actor's Equity Association.",
      },
    },
    photo: {
      pho01: {
        nameF: "",
        nameL: "",
        image: "2014-ta_Aeneid-2741.jpg",
      },
      pho02: {
        nameF: "",
        nameL: "",
        image: "2014-ta_Aeneid-2845.jpg",
      },
      pho03: {
        nameF: "",
        nameL: "",
        image: "2014-ta_Aeneid-2944.jpg",
      },
      pho04: {
        nameF: "",
        nameL: "",
        image: "2014-ta_Aeneid-2978.jpg",
      },
      pho05: {
        nameF: "",
        nameL: "",
        image: "2014-ta_Aeneid-3083.jpg",
      },
      pho06: {
        nameF: "",
        nameL: "",
        image: "2014-ta_Aeneid-3446.jpg",
      },
      pho07: {
        nameF: "",
        nameL: "",
        image: "2014-ta_Aeneid-3544.jpg",
      },
      pho08: {
        nameF: "",
        nameL: "",
        image: "2014-ta_Aeneid-3660.jpg",
      },
    },
    criti: {
      cri01: {
        nameF: "Anna",
        nameL: "Furstenberg",
        title: "Rover Arts",
        words: "Ancient epic relevant, fascinating, moving.",
        dates: "March 8, 2014",
        links: "",
      },
      cri02: {
        nameF: "Christian",
        nameL: "St-Pierre",
        title: "Le Devoir",
        words: "Marionnette en exil.",
        dates: "March 8, 2014",
        links: "2014-aeneid-01.pdf",
      },
      cri03: {
        nameF: "Julie",
        nameL: "Cler",
        title: "Info-culture",
        words: "<em>The Aeneid</em>, flash sur le monde.",
        dates: "March 9, 2014",
        links: "2014-aeneid-02.pdf",
      },
      cri04: {
        nameF: "Katheryn",
        nameL: "Greenway",
        title: "The Gazette",
        words: "Puppets guide <em>The Aeneid</em>'s epic voyage--Virgil's classic tale of a man in search of his homeland still has meaning today.",
        dates: "March 8, 2014",
        links: "",
      },
      cri05: {
        nameF: "Jeanette",
        nameL: "Kelly",
        title: "Cinq a six (CBC Radio)",
        words: "<em>The Aeneid</em> : Exploring the refugee experience... with puppets'. This week's show is all about outsider storytelling, and how the sense that you don't belong to the mainstream can make for some powerful artistic statements.",
        dates: "March 8, 2014",
        links: "2014-aeneid-03.pdf",
      },
    },
    logos: {
      log01: {
        image: "CCFA_RGB_colour_e.jpg",
      },
      log02: {
        image: "Calq_cyan.jpg",
      },
      log03: {
        image: "logo-cole.jpg",
      },
      log04: {
        image: "CMYK_Logo_CAM+Montreal.jpg",
      },
    },
  },
  {
    pyear: "2012",
    dateE: "October 11-20th 2012",
    dateF: "11-20 octobre, 2012",
    place: "La Chapelle Theatre",
    title: "The Medea Effect",
    image: "poster-2012_medea1.jpg",
    bannE: "banner-2012_en_medea1.jpg",
    bannF: "banner-2012_fr_medea1.jpg",
    synop: {
      texts: {
        par01: "Ada is a mother who has forgotten her child during an emotional trauma. Ugo's childhood fear has come to pass, his mother has forgotten him. <em>The Medea Effect</em> is an emotional roller-coaster ride launching the audience into abstract heights and plunging them into emotional depths. It is also an emotional tug-of-war between two isolated minds over the same subject: the mother.",
        par02: "Written by Quebecois playwright Suzie Bastien, translated for Talisman Theatre by Nadine Desrochers, and directed by Emma Tibaldo, <em>The Medea Effect</em> has never been produced in English in Montreal! Opening October 11,2012 at La Chapelle, <em>The Medea Effect</em> will star Jennifer Morehouse as Ada and Éloi ArchamBaudoin as Ugo. The Medea Effect is like quick-sand--the unwitting audience member is lured into its emotional quagmire by the familiar surface of simple speech and common empathy. Simple speech is revealed to be a complex system of references with no inside nor outside, no beginning nor end. Once trapped, the audience is drawn inexorably into a region of profound philosophical inquiry.",
      },
    },
    auths: {
      aut01: {
        nameF: "Suzie",
        nameL: "Bastien",
      },
    },
    trans: {
      tra01: {
        nameF: "Nadine",
        nameL: "Desrochers",
      },
    },
    drama: {
      dra01: {
        nameF: "",
        nameL: "",
      },
    },
    direc: {
      dir01: {
        nameF: "Emma",
        nameL: "Tibaldo",
      },
    },
    chore: {
      cho01: {
        nameF: "",
        nameL: "",
      },
    },
    actor: {
      act01: {
        nameF: "Éloi",
        nameL: "ArchamBaudoin",
        chara: "Ugo",
      },
      act02: {
        nameF: "Jennifer",
        nameL: "Morehouse",
        chara: "Ada",
      },
    },
    desig: {
      des01: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Set",
        titlF: "Décor",
      },
      des02: {
        nameF: "Fruzsina",
        nameL: "Lànyi",
        titlE: "Costumes",
        titlF: "Costume",
      },
      des03: {
        nameF: "David",
        nameL: "Perreault Ninacs",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des04: {
        nameF: "Matthew",
        nameL: "Waddell",
        titlE: "Sound",
        titlF: "Son",
      },
      des05: {
        nameF: "Michael",
        nameL: "Leon",
        titlE: "Composer",
        titlF: "Compositeur",
      },
      des06: {
        nameF: "Rasili",
        nameL: "Botz",
        titlE: "Movement",
        titlF: "Mouvement",
      },
      des07: {
        nameF: "Jean",
        nameL: "Ranger",
        titlE: "Video",
        titlF: "Vidéo",
      },
    },
    copro: {
      texts: {
        par01: "",
      },
    },
    photo: {
      pho01: {
        nameF: "",
        nameL: "",
        image: "2012-tme1_12.jpg",
      },
      pho02: {
        nameF: "",
        nameL: "",
        image: "2012-tme1_12.jpg",
      },
      pho03: {
        nameF: "",
        nameL: "",
        image: "2012-tme_DSCF3726.jpg",
      },
    },
    criti: {
      cri01: {
        nameF: "Pat",
        nameL: "Donnelly",
        title: "The Gazette",
        words: "Suzie Bastien's <em>The Medea Effect</em>, however, is an exception. It's contemporary and universal, inspired by Pirandello as well as Euripides. And it offers terrific acting opportunities to a mature woman who knows how to command the stage and a younger man who knows how to hold his own against her. [...] This is actor's theatre (with director Tibaldo as rigorous coach) that hits the motherlode of gravitas required for tragedy. Morehouse is a marvel. Catharsis delivered. Bravo!",
        dates: "October 18, 2012",
        links: "2012-medea-01.pdf",
      },
      cri02: {
        nameF: "",
        nameL: "",
        title: "Westmount Examiner",
        words: "<em>The Medea Effect</em> shimmers with the sparks and flashes of profound insight penned over two thousand years ago but refracted and focused through a contemporary lens.",
        dates: "October 18, 2012",
        links: "",
      },
      cri03: {
        nameF: "Pat",
        nameL: "Donnelly",
        title: "The Gazette (preview)",
        words: "Talisman Theatre actually specializes in producing English translations of French-language works. Its mandate is to bring 'the visceral intensity of Québécois theatrical practices to non-francophones.' [...]The Medea Effect is little known in francophone Montreal, having only been produced in French in Quebec City, directed by Marie Gignac, in 2005. [...] Thus Talisman, which was founded in 2005 by Emma Tibaldo and current artistic director Lyne Paquette, is encouraging Montreal audiences to embrace a playwright (Bastien) who is respected in Quebec literary/theatrical circles--and in Europe--but hasn't yet found her place on Montreal stages.",
        dates: "October 18, 2012",
        links: "",
      },
      cri04: {
        nameF: "Emma",
        nameL: "Overton",
        title: "Cult#MTL",
        words: "Talisman Theatre's <em>The Medea Effect</em>... is a play that sticks the knife in and twists. ...Nadine Desrochers' seamless translation affords English-speaking audiences access to this dark tale, and makes <em>The Medea Effect</em> a profound example of the cultural experience Talisman offers to the theatre-going community of Montreal.",
        dates: "October 16, 2012",
        links: "2012-medea-02.pdf",
      },
      cri05: {
        nameF: "Bernard",
        nameL: "Wheeley",
        title: "Voir Montreal",
        words: "C'est le théâtre dans le théâtre. La démonstration que le théâtre influence des vies. Voilà qui est très intéressant. Deux personnages, Ugo et Ada reconnaissent l'effet de Médée sur eux. Suzie Bastien offre un texte riche, très humain. Une belle matière pour des comédiens de talent. Jennifer Morehouse (Ada) impressionne par la profondeur de son jeu. Elle porte sur ses épaules tous les malheurs, toutes les douleurs, toutes les blessures qui ont marqué son personnage, Morehouse a une très forte présence sur scène, présence à laquelle Eloi ArchamBaudoin (Ugo) répond de belle façon par un jeu énergique et nuancé.",
        dates: "October 18, 2012",
        links: "",
      },
      cri06: {
        nameF: "Daniela",
        nameL: "Smith Fernandez",
        title: "Bloody Underrated",
        words: "Deceptively, the show appears at first to be a rudimentary allegory... instead, the play addresses these expectations dead on, and then continues to slowly unravel the layers of cliches and simplistic explanations. ... This transition towards the intimate and personal is represented through a shift from naturalistic dialogue to highly stylized and expressive movements, due in no small part to some achingly beautiful sound and lighting design by Matthew Waddell and David Perreault Ninacs. ... This is a polished piece that reaches profound levels of psychological complexity. ... That said, it's an innovative professional show with great performances, and one that makes use of all the elements in its theatrical toolbox.",
        dates: "October 18, 2012",
        links: "",
      },
      cri07: {
        nameF: "Lois",
        nameL: "Brown",
        title: "",
        words: "I don't think there have been any reviews yet - but here are some from people who have seen it 'C'était une magistrale première!', 'ce texte incroyable', 'Haunting' and from me: 'A tough, and lyrical rendering of an intense text.' Talisman's work with translation is rigourous, yet Emma and Lynn bring a singular and strong aesthetic to the text. And the exploration is new. Important work - important to see.",
        dates: "October 18, 2012",
        links: "",
      },
      cri08: {
        nameF: "Joel",
        nameL: "Fishbane",
        title: "Charpo-Canada",
        words: "Emma Tibaldo has never been shy when it comes to her thoughts on Medea, the famed murderess of Greek myth. 'She's always been a hero to me,' says the artistic director of Playwrights Workshop Montreal, one of Canada's longest-running play development centres. It's a bold statement, given that Medea murders her own children to enact revenge on a faithless husband. But Emma Tibaldo sees it as something more then just an act of infanticide. 'She's a hero to me because of her willingness to do something extra-ordinary to get justice.",
        dates: "October 18, 2012",
        links: "",
      },
      cri09: {
        nameF: "Hannah",
        nameL: "Liddle",
        title: "",
        words: "Fortunately the lights were off and no one was looking at my face, or they would have seen a gaping mouth and furrowed brow. I was on the edge of my chair and I couldn't help it, only changing my position once to kick my coffee over and receive a look of disdain from the guy next to me. [...] I could hardly wrap my head around the play without wanting to burst into tears. Likely, that sense of overwhelming is the effect of Medea, and I only became one of her many subjects.",
        dates: "October 18, 2012",
        links: "",
      },
      cri10: {
        nameF: "Anna",
        nameL: "Fuerstenborg",
        title: "",
        words: "The play Medea has always been problematic; it involves betrayal of epic proportions and tragedy that is earth shattering. ...But Jennifer Morehouse gives an amazing performance...",
        dates: "October 18, 2012",
        links: "",
      },
      cri11: {
        nameF: "Brendan",
        nameL: "Kelly",
        title: "@cbcdaybreak",
        words: "Tibaldo: 'It is an extraordinary story! Suzie Bastien allows us to accept the unacceptable through this play. She makes us understand why this woman does what she does and she also makes us understand why we are so fascinated with Medea. Also, from a Director's point of view: Why is this story so important? Why do we choose to go see it? Why do we like to see a trainwreck? Why do we want to find out the horrible things about humanity? What is it that drives us to that point? And once we have the information: What do we do with it? What do we do with that emotional catharsis? Where do we go with the pain that we carry when we find out about these stories?",
        dates: "October 18, 2012",
        links: "",
      },
      cri12: {
        nameF: "Pat",
        nameL: "Donnelly",
        title: "The Gazette (preview)",
        words: "Meanwhile, Talisman Theatre launched <em>The Medea Effect</em>, a Quebec take on Medea by Suzie Bastien, translated into English by Nadine Desrochers, Thursday, Oct. 11 at Théâtre La Chapelle. ...<em>The Medea Effect</em> is a deconstruction of the Greek play, twice removed by translation...",
        dates: "October 18, 2012",
        links: "",
      },
      cri13: {
        nameF: "Francois",
        nameL: "Nadeau",
        title: "Le Messager Verdun",
        words: "C'est la première fois de la Verdunoise [Jennifer Morehouse] foulera les planches avec la compagnie Talisman Theatre. Cette dernière a travaillé en télévision, théâtre, cinéma et doublage de films d'animation depuis une vingtaine d'années. Elle a obtenu plusieurs nominations au cours de sa carrière, en particulier du côté du théâtre, incluant une citation pour le prix Dora et deux pour le prix Betty Mitchell.",
        dates: "October 18, 2012",
        links: "",
      },
    },
    logos: {
      log01: {
        image: "CCFA_RGB_colour_e.jpg",
      },
      log02: {
        image: "Calq_cyan.jpg",
      },
      log03: {
        image: "logo-cole.jpg",
      },
      log04: {
        image: "CMYK_Logo_CAM+Montreal.jpg",
      },
    },
  },
  {
    pyear: "2011",
    dateE: "October 20-29, 2011",
    dateF: "20-29 octobre, 2011",
    place: "Studio Jean Valcourt",
    title: "Coma Unplugged",
    image: "poster-2011_coma.jpg",
    bannE: "banner-2011_en_coma.jpg",
    bannF: "banner-2011_fr_coma.jpg",
    synop: {
      texts: {
        par01: "As a recently divorced humour columnist for the city's most read newspaper, Daniel is a local celebrity. For Éloi ArchamBaudoin who plays Daniel, the character contemplates the important things we all should have on our mind, from the notion of success to the excess of oversized vehicles, 'This is a very modern piece set in present day, with references to recent events and Montreal news.' <em>Coma Unplugged</em> is a witty and beautifully touching look inside Daniel's mind. Here, we meet his various 'visitors': his ex-wife Marjorie (Braganza), Roger- a childhood bully (Reiter), his mother Madeleine (Glover), Ishouad- a mysterious Tuareg warrior (Miller) and Stephen Hawking. Daniel ultimately grasps that he is comatose following a traffic accident. Or was it an accident? In his own words, he wanted 'to see if I could be as brave as that little Chinese guy in Tiananmen Square'. His daughter needs him, but will he choose to return? This is the reality check that leads him to examine the cynicism inherent to his era. Teetering between life and death, he finds clarity.",
      },
      texts: {
        par02: "With <em>Coma Unplugged</em> we bring the audience into the mind of a man wrestling with inner demons invoked by cultural conflict. As this remarkably ordinary delirium unfolds, it reveals a mind of sincerity, perseverance, and self-awareness. Cloaked under comedy and cunning, Pierre-Michel's text is nevertheless an earnest and sage message for all generations: cherish life and love, because sardonic indifference leads only to emptiness and apathy. As morbidly amusing as Daniel's grave circumstances may be, the audience is impelled to contemplate their own values and discover a startling empathy.",
      },
    },
    auths: {
      aut01: {
        nameF: "Pierre-Michel",
        nameL: "Tremblay",
      },
    },
    trans: {
      tra01: {
        nameF: "Micheline",
        nameL: "Chevrier",
      },
    },
    drama: {
      dra01: {
        nameF: "",
        nameL: "",
      },
    },
    direc: {
      dir01: {
        nameF: "Zach",
        nameL: "Fraser",
      },
    },
    chore: {
      cho01: {
        nameF: "",
        nameL: "",
      },
    },
    actor: {
      act01: {
        nameF: "Éloi",
        nameL: "ArchamBaudoin",
        chara: "Daniel",
      },
      act02: {
        nameF: "Glenda",
        nameL: "Braganza",
        chara: "Marjorie",
      },
      act03: {
        nameF: "Susan",
        nameL: "Glover",
        chara: "Madeleine",
      },
      act04: {
        nameF: "Chimwemwe",
        nameL: "Miller",
        chara: "Ishouad",
      },
      act05: {
        nameF: "Donovan",
        nameL: "Reiter",
        chara: "Roger",
      },
    },
    desig: {
      des01: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Set",
        titlF: "Décor",
      },
      des02: {
        nameF: "Fruzsina",
        nameL: "Lànyi",
        titlE: "Costumes",
        titlF: "Costume",
      },
      des03: {
        nameF: "David",
        nameL: "Perreault Ninacs",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des04: {
        nameF: "Matthew",
        nameL: "Waddell",
        titlE: "Sound",
        titlF: "Son",
      },
    },
    copro: {
      texts: {
        par01: "",
      },
    },
    photo: {
      pho01: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2011-cu_DSCF3109.jpg",
      },
      pho02: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2011-cu_DSCF3201.jpg",
      },
      pho03: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2011-cu_DSCF3231.jpg",
      },
      pho04: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2011-cu_DSCF3248.jpg",
      },
      pho05: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2011-cu_DSCF3260.jpg",
      },
      pho06: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2011-cu_DSCF3340.jpg",
      },
      pho07: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2011-cu_DSCF3340b.jpg",
      },
      pho08: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2011-cu_DSCF3353.jpg",
      },
      pho09: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2011-cu_DSCF3381a.jpg",
      },
      pho10: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2011-cu_DSCF3396.jpg",
      },
      pho11: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2011-cu_DSCF3399.jpg",
      },
      pho12: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2011-cu_DSCF3402.jpg",
      },
      pho13: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2011-cu_DSCF3447a.jpg",
      },
      pho14: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2011-cu_DSCF3447c.jpg",
      },
      pho15: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2011-cu_DSCF3452.jpg",
      },
      pho16: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2011-cu_DSCF3532.jpg",
      },
    },
    criti: {
      cri01: {
        nameF: "Pat",
        nameL: "Donnelly",
        title: "The Gazette",
        words: "Fraser and company have tuned in to Tremblay's wacky wavelength in a way that makes <em>Coma Unplugged</em> hilarious as well as timely, intelligent and provocative. [...] At about one hour and 45 minutes, <em>Coma Unplugged</em> could use some trimming. But make no mistake: Talisman Theatre has a hit on its hands. Get your tickets now.",
        dates: "October 21, 2011",
        links: "2011-coma-03.pdf",
      },
      cri02: {
        nameF: "Pat",
        nameL: "Case",
        title: "The Concordian",
        words: "<em>Coma Unplugged</em> is hilarious and poignant... ArchamBaudoin deftly handles the embittered Daniel... Braganza summons Marjorie's desperation, rage and frustrated love with ease, burning brilliantly... Glover gives a flawless performance as a doting, talkative mother... Chimwemwe Miller gives a solid performance... The hilarity of Donovan Reiter's character belies a wounded, frustrated divorcee... Perhaps most notable is the elaborate set designed by Lyne Paquette. ...<em>Coma Unplugged</em> is a contemporary jewel.",
        dates: "October 26, 2011",
        links: "2011-coma-01.pdf",
      },
      cri03: {
        nameF: "Anna",
        nameL: "Fuerstenberg",
        title: "RoverArts",
        words: "The acting was flawless. Eloi Archambaudoin was riveting as Daniel. ...Glenda Braganza was spectacular as Marjorie and her performance made the play come alive. ...Susan Glover gets comedy, and ...was manic to within an inch of credibility and understood with great instinct the need for 'set up and delivery'. ...Chimwemwe Miller had the thankless part of Ishouad, the Twarek [sic] warrior, but his physical presence was enormously charming and worth watching. ...Donovan Reiter ... played his macho gazpacho part with great energy. ...The set was terrific ...Furthermore, the lighting and sound were great and the costumes worked very well. ...Talisman should be congratulated for their efforts, especially on the fantastic cast.",
        dates: "October 21, 2011",
        links: "",
      },
      cri04: {
        nameF: "Marilou",
        nameL: "Craft",
        title: "PLEIN ESPACE",
        words: "L'équipe de Talisman Theatre propose somme toute une production respectueuse de la vision de l'auteur. Tremblay suggérait que l'action se déroule dans l'appartement de Daniel, un décor « pas réaliste, plutôt cubiste, surréaliste en point de fuite, post-traumatique, petitement apocalyptique », proposition que l'équipe de concepteurs respecte à la lettre. Le décor, une structure imposante et anarchique, permet des entrées en scène surprenantes et comiquement inusitées. Le détail des accessoires et des costumes est réjouissant, travaillé avec le souci évident de créer un univers « cohérent avec l'inconscient de Daniel Martin », selon la sommation de l'auteur.",
        dates: "October 21, 2011",
        links: "",
      },
      cri05: {
        nameF: "Cecile",
        nameL: "Mouly",
        title: "Citeeze Montreal",
        words: "A first-class cast drives us through a multiplicity of emotions. ...Éloi Archambaudin plays Daniel exquisitely. ...Pierre Michel Tremblay's English translation has been beautifully written by Micheline Chevrier.",
        dates: "October 21, 2011",
        links: "",
      },
      cri06: {
        nameF: "Chris",
        nameL: "Liu",
        title: "McGill Tribune",
        words: "It's a terrible thing to watch a mind go to waste. Yet Pierre-Michel Tremblay's <em>Coma Unplugged</em> makes it so infectiously fun... Talisman Theatre's latest production is proof that when you mix a sharply written script with a cast whose energy knows no bounds, magic occurs. [...] <em>Coma Unplugged</em> makes the most out of its discombobulation. The voyage through Daniel's fracturing mind is one fraught with side-splitting laughter and deep introspection: in other words, a perfect night out.",
        dates: "October 21, 2011",
        links: "",
      },
      cri07: {
        nameF: "Bernard",
        nameL: "Wheeley",
        title: "Voir.ca",
        words: "Am I a failure?' Voilà la question existentielle à laquelle doit répondre Daniel Martin (Éloi ArchamBaudoin) avant de décider de revenir ou non à la Vie. ...Je m'en voudrais de passer sous silence la superbe prestation D'Éloi ArchamBaudoin. ...Ayant vu les deux versions de <em>Coma Unplugged</em>, je peux affirmer que celle-ci m'a davantage amené au coeur du drame. J'ai bien apprécié. On peut le dire sans crainte, « this show is not a failure ».",
        dates: "October 21, 2011",
        links: "",
      },
      cri08: {
        nameF: "Estelle",
        nameL: "Rosen",
        title: "The Charlebois Post (Canada)",
        words: "Director Zach Fraser sees breaking the boundaries of reality as an opportunity to present a story both comic and potentially tragic. [...] A reflective play with more depth than we would initially expect - humourous with an underlying edge - touches on meaningful issues without hitting you over the head with a message. [...] Kudos to Éloi ArchamBaudoin for eloquently embodying the role of Daniel Martin.",
        dates: "October 26, 2011",
        links: "2011-coma-01.pdf",
      },
      cri09: {
        nameF: "Al",
        nameL: "Lafrance",
        title: "Bloody Underrated",
        words: "From the moment I walked into the room, I loved what I saw. Lyne Paquette did a great job with this set... This space was used incredibly well throughout the play... They put on a great show, brought huge amounts of energy to the room, and made this 95 minute play feel like it took place in about 20. ...Tickets are 25$... and they're definitely worth every penny. This is one of my favourite plays of the year, easily, and I hope you all go see it.",
        dates: "October 21, 2011",
        links: "",
      },
      cri10: {
        nameF: "",
        nameL: "",
        title: "",
        words: "Thanks to @gcharlebois and @CharPoCanada their Tweets: 'Review: Coma Unplugged, gorgeous play with magnificent lead actor'; 'Review: (Montreal) Coma Unplugged - brilliant lead actor in this mix of reality, dreams and imagination'.",
        dates: "",
        links: "",
      },
    },
    logos: {
      log01: {
        image: "CCFA_RGB_colour_e.jpg",
      },
      log02: {
        image: "Calq_cyan.jpg",
      },
      log03: {
        image: "logo-cole.jpg",
      },
      log04: {
        image: "CMYK_Logo_CAM+Montreal.jpg",
      },
    },
  },
  {
    pyear: "2010",
    dateE: "October 14-23, 2010",
    dateF: "14-23 octobre, 2010",
    place: "La Chapelle Theatre",
    title: "The Flood Thereafter",
    image: "poster-2010_flood.jpg",
    bannE: "banner-2010_en_flood.jpg",
    bannF: "banner-2010_fr_flood.jpg",
    synop: {
      texts: {
        par01: "<em>The Flood Thereafter</em> is a twisted fairytale set in a tidal pool of a fishing village on the Lower St-Laurence. It tells June's story, daughter of Grace the owner of the local restaurant, who cursed the small community and drove away the women of the village. Now, when June strips at Bar Émotion the men weep helplessly. But June tires of carrying the fishermens' burdens. When the trucker Denis appears, he breaks the spell holding back a tide.",
      },
    },
    auths: {
      aut01: {
        nameF: "Sarah",
        nameL: "Berthiaume",
      },
    },
    trans: {
      tra01: {
        nameF: "Nadine",
        nameL: "Desrochers",
      },
    },
    drama: {
      dra01: {
        nameF: "",
        nameL: "",
      },
    },
    direc: {
      dir01: {
        nameF: "Emma",
        nameL: "Tibaldo",
      },
    },
    chore: {
      cho01: {
        nameF: "",
        nameL: "",
      },
    },
    actor: {
      act01: {
        nameF: "Stephane",
        nameL: "Blanchette",
        chara: "",
      },
      act02: {
        nameF: "Catherine",
        nameL: "Colvey",
        chara: "Grace",
      },
      act03: {
        nameF: "Chimwemwe",
        nameL: "Miller",
        chara: "Denis",
      },
      act04: {
        nameF: "Bill",
        nameL: "Rowat",
        chara: "",
      },
      act05: {
        nameF: "Amelia",
        nameL: "Sargisson",
        chara: "June",
      },
      act06: {
        nameF: "Felicia",
        nameL: "Shulman",
        chara: "Penelope",
      },
    },
    desig: {
      des01: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Set",
        titlF: "Décor",
      },
      des02: {
        nameF: "Fruzsina",
        nameL: "Lànyi",
        titlE: "Costumes",
        titlF: "Costume",
      },
      des03: {
        nameF: "David",
        nameL: "Perreault Ninacs",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des04: {
        nameF: "Michael",
        nameL: "Leon",
        titlE: "Sound",
        titlF: "Son",
      },
      des05: {
        nameF: "Rasili",
        nameL: "Botz",
        titlE: "Movement",
        titlF: "Mouvement",
      },
    },
    copro: {
      texts: {
        par01: "",
      },
    },
    photo: {
      pho01: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2010-tft_DSCF2400.jpg",
      },
      pho02: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2010-tft_DSCF2470.jpg",
      },
      pho03: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2010-tft_DSCF2548.jpg",
      },
      pho04: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2010-tft_DSCF2676.jpg",
      },
      pho05: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2010-tft_DSCF2686.jpg",
      },
      pho06: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2010_tft_DSCF2466.jpg",
      },
    },
    criti: {
      cri01: {
        nameF: "Maurice",
        nameL: "Podbrey",
        title: "",
        words: "The whole production was delightful--a thoroughly enchanting evening. The playwright has achieved something rarely seen in the English language theate--a story told in magical terms and brilliantly excecuted by the director and designers and the performers. My sincere thanks to all.",
        dates: "October 20, 2010",
        links: "",
      },
      cri02: {
        nameF: "Walter",
        nameL: "Lyng",
        title: "The Suburban",
        words: "Few fairy tales are inspired by small town strip clubs but when playwright Sarah Berthiaume was on a recent road trip, this is exactly what happened. ...Despite being a fantasy on the surface, Tibaldo says the aim of the play is to deal with real world issues. '[I'm trying to] begin a conversation about what stripping means to women, to the world, to men and how we deal with it,' she says.",
        dates: "October 20, 2010",
        links: "2010-tft-suburban.pdf",
      },
      cri03: {
        nameF: "Thea",
        nameL: "",
        title: "Orcasound",
        words: "The set, designed by Lyne Paquette was extremely interesting visually. On its own, it looks like the broken hull of a shipwreck, with the different pieces representing different locations in town. The floor was scattered with kelp-like 'wigs' that Homer's wife, Penelope (Felicia Shulman) was often working on in the background, and that June would pose with when she stripped.",
        dates: "October 20, 2010",
        links: "2010-tft-orcasound.pdf",
      },
      cri04: {
        nameF: "Val",
        nameL: "Cardinal",
        title: "The Concordian",
        words: "Talisman Theatre specializes in bringing Québécois theatre to the anglophone stage. The show is more risqué than most English productions, and definitely not for those shocked by nudity, both male and female. However, the nudity is never gratuitous and unnecessary; it emphasizes the themes of the mystical show, which is well worth a look.",
        dates: "October 19, 2010",
        links: "2010-tft-concordian.pdf",
      },
      cri05: {
        nameF: "Bernard",
        nameL: "Wheeley",
        title: "(Blog)",
        words: "Je reviens du spectacle <em>The Flood Thereafter</em> ému, touché par cette fable de Sarah Berthiaume magnifiquement bien traduite par Nadine Desrochers, qui a eu l'intelligence de laisser, ici et là, du français dans les moments les plus touchants. J'y ai vu trois superbes comédiennes : Amélia Sargisson, Catherine Colvey (magique) et Felicia Shulman. Les comédiens masculins ont également bien joués.",
        dates: "October 20, 2010",
        links: "",
      },
      cri06: {
        nameF: "Kent",
        nameL: "Stetson",
        title: "GG Award-winner",
        words: "What's the syndrome where people faint in front of great paintings? You knocked me completely off-kilter. Again. Beautiful direction... note/tone/colour perfect. Great lighting. And fabulous seaweed. Loved the human/bird/fish/ship-rib set.",
        dates: "October 20, 2010",
        links: "",
      },
      cri07: {
        nameF: "Amie",
        nameL: "",
        title: "Midnight Poutine",
        words: "It may take a long time for this story to feel like a fairy tale, but once the magic kicks in you'll be held in a mermaid's snares until the end.",
        dates: "October 16, 2010",
        links: "2010-tft-poutine.pdf",
      },
      cri08: {
        nameF: "Robyn",
        nameL: "Fadden",
        title: "The Hour",
        words: "Twisted fairytale meets feminist theory in a fishing village on the lower St-Lawrence River where all the women have been driven away...",
        dates: "October 20, 2010",
        links: "",
      },
      cri09: {
        nameF: "MJ",
        nameL: "Stone",
        title: "The Hour",
        words: "Post-modern and poetic, Sarah Berthiaume has weaved an oddly compelling and maddening scenario that is as murky and bold as the wildest fisherman's tale. Much of the play's intent is revealed between the lines and the ta-da moment, when everything suddenly made sense, didn't hit me until hours after.",
        dates: "October 16, 2010",
        links: "2010-tft-hour.pdf",
      },
      cri10: {
        nameF: "Christine",
        nameL: "Long",
        title: "CTV",
        words: "The Flood Thereafter adds class to small-town strip bars when it equates the dancers with mermaids and how both know how to entrance a man.",
        dates: "October 20, 2010",
        links: "",
      },
      cri11: {
        nameF: "Yves",
        nameL: "Rousseau",
        title: "Le Quatrième",
        words: "Avec <em>Le Déluge après</em>, Sarah Berthiaume interroge de mythologie la féminine identité, dans une iconoclaste tragicomédie métaphorique et poétique bien d'aujourd'hui.",
        dates: "October 17, 2010",
        links: "2010-tft-quatrieme.pdf",
      },
      cri12: {
        nameF: "Evelyn",
        nameL: "Reid",
        title: "Montreal Theatre Guide",
        words: "[It] is way out there as a modern-day fairytale, delightfully so, opting out of the usual cookie-cutter good versus evil ideology typical of the folktale genre.",
        dates: "October 16, 2010",
        links: "2010-tft-mtr.pdf",
      },
      cri13: {
        nameF: "Pat",
        nameL: "Donnelly",
        title: "The Gazette",
        words: "This reviewer is not a fan of staged poetry, with exceptions made for the likes of William Shakespeare, Dylan Thomas and Carson McCullers. Few writers can write a good play and good poetry at the same time. Berthiaume, a recent theatre school graduate, has certainly made a game try. She has a flair for imagery and lyricism.",
        dates: "October 21, 2010",
        links: "2010-tft-gazette1.pdf",
      },
      cri14: {
        nameF: "Pat",
        nameL: "Donnelly",
        title: "The Gazette (preview)",
        words: "Talisman Theatre was born to build bridges. Its mandate is to introduce new French-language Quebec plays to anglophone audiences via translation. [...<em>The Flood Thereafter</em>] tells a tale inspired by the sad and sordid reality of women who work in Quebec's numerous small-town strip bars. [...] <em>The Flood Thereafter</em>, however, doesn't deal in strip-bar realism. [...] In <em>The Flood Thereafter</em>, June, the exotic dancer who makes men weep, is part mermaid.",
        dates: "October 15, 2010",
        links: "2010-tft-gazette2.pdf",
      },
      cri15: {
        nameF: "Alex",
        nameL: "Woolcott",
        title: "Rover Arts",
        words: "The play is reminiscent of a Shakespearian romance--those strange plays that exist somewhere between comedy and tragedy--and so it is appropriate that the text seesaws between heightened poetic language and rougher, more colloquial prose.",
        dates: "October 16, 2010",
        links: "2010-tft-rover.pdf",
      },
    },
    logos: {
      log01: {
        image: "CCFA_RGB_colour_e.jpg",
      },
      log02: {
        image: "Calq_cyan.jpg",
      },
      log03: {
        image: "logo-cole.jpg",
      },
      log04: {
        image: "CMYK_Logo_CAM+Montreal.jpg",
      },
    },
  },
  {
    pyear: "2009",
    dateE: "October 6-17, 2009",
    dateF: "6-17 octobre, 2009",
    place: "Centaur Theatre",
    title: "Rock, Paper, Jackknife...",
    image: "poster-2009_rock.jpg",
    bannE: "banner-2009_en_rock.jpg",
    bannF: "banner-2009_fr_rock.jpg",
    synop: {
      texts: {
        par01: "A shipment of alcohol is dumped on the icy shore of the far north, along with four young stowaways. The refugees are unable to communicate with the inhabitants of the remote settlement, which forces onto the local nurse, who speaks their language, the responsibility for their integration. Neither the refugees nor the introverted tutor can find respite from the clutches of isolation, suffering, and boredom leading them to seek a means of escape. Largely narrated through childlike metaphors, and similes, the play explores their struggle in a destabilizing and deeply emotional way. The opening and closing scene is as bleak as any in Wuthering Heights complete with the wind knocking something at the door, blood on the bedclothes, and an otherworldly voice saying 'I will come home'. Subtley, in the guise of Ali's ten-year-old girl's voice, <em>Rock, Paper, Jackknife...</em> pierces the audiences' defences, touching a nerve of immediacy rarely touched. Ali's narration, though unsophisticated, is allegorical, rich in poetry, and filled with a looming presence.",
      },
    },
    auths: {
      aut01: {
        nameF: "Marilyn",
        nameL: "Perreault",
      },
    },
    trans: {
      tra01: {
        nameF: "Nadine",
        nameL: "Desrochers",
      },
    },
    drama: {
      dra01: {
        nameF: "",
        nameL: "",
      },
    },
    direc: {
      dir01: {
        nameF: "Emma",
        nameL: "Tibaldo",
      },
    },
    chore: {
      cho01: {
        nameF: "",
        nameL: "",
      },
    },
    actor: {
      act01: {
        nameF: "Julie",
        nameL: "Tamiko Manning",
        chara: "Mielke",
      },
      act02: {
        nameF: "Rockne",
        nameL: "Corrigan",
        chara: "Nox",
      },
      act03: {
        nameF: "Stefanie",
        nameL: "Buxton",
        chara: "Ali",
      },
      act04: {
        nameF: "Alex",
        nameL: "McCooeye",
        chara: "Taymore",
      },
      act05: {
        nameF: "Lucinda",
        nameL: "Davis",
        chara: "Sola",
      },
    },
    desig: {
      des01: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Set",
        titlF: "Décor",
      },
      des02: {
        nameF: "Fruzsina",
        nameL: "Lanyi",
        titlE: "Costumes",
        titlF: "Costume",
      },
      des03: {
        nameF: "David",
        nameL: "Perreault Ninacs",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des04: {
        nameF: "Michael",
        nameL: "Leon",
        titlE: "Sound",
        titlF: "Son",
      },
      des05: {
        nameF: "Rasili",
        nameL: "Botz",
        titlE: "Movement",
        titlF: "Mouvement",
      },
    },
    copro: {
      texts: {
        par01: "This translation is commissioned and developed by Playwrights' Workshop Montreal's Cole Foundation Emerging Translator program.",
      },
    },
    photo: {
      pho01: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2009-rpjk_DSCF1629.jpg",
      },
      pho02: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2009-rpjk_DSCF1646.jpg",
      },
      pho03: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2009-rpjk_DSCF1657.jpg",
      },
      pho04: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2009-rpjk_DSCF1680.jpg",
      },
      pho05: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2009-rpjk_DSCF1726.jpg",
      },
      pho06: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2009-rpjk_DSCF1762.jpg",
      },
      pho07: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2009-rpjk_DSCF1781.jpg",
      },
      pho08: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2009-rpjk_DSCF1820.jpg",
      },
      pho09: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2009-rpjk_DSCF1827.jpg",
      },
      pho10: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2009_rpjk_DSCF1841.jpg",
      },
    },
    criti: {
      cri01: {
        nameF: "",
        nameL: "",
        title: "Talisman Theatre",
        words: "We are pleased to announce that our English-language premiere of Marilyn Perreault's <em>Rock, Paper, Jackknife...</em> translated by Nadine Desrochers, has been nominated for the 2009 ACQT prize in the category 'Théâtre anglophone.'",
        dates: "October 25, 2009",
        links: "2009-rpjk-voir.pdf",
      },
      cri02: {
        nameF: "Andrew",
        nameL: "Cuk",
        title: "John Abbott College",
        words: "Recently, I took a group of my theatre students to see <em>Rock, Paper, Jackknife...</em>. This play presents us a difficult, disturbing and beautifully poetic text, as Perreault attempts to create a new hybrid language for her characters to traverse the labyrinth of their dysfunction. In the discussing the play in the subsequent week in class, the students grappled with how the author is asking us to question the very nature of language, and--even though the play was set in a nebulous time and place--she is making a comment on a volatile political and cultural topic that affects each of my students' daily lives. I was once again struck how vital a medium theatre is to our society.",
        dates: "October 25, 2009",
        links: "",
      },
      cri03: {
        nameF: "Brett",
        nameL: "Hooton",
        title: "The Hour",
        words: "The world is full of people who spend their time quietly doing amazing things. ...Emma Tibaldo is one of these subtle superheroes. ...Leaping building-sized themes is what theatre does best, but <em>Rock, Paper, Jackknife...</em> goes even further. The script stretches language itself by twisting grammar and using words in unconventional ways. The result is a more visceral and innocent form of expression.",
        dates: "October 1, 2009",
        links: "2009-rpjk-hour.pdf",
      },
      cri04: {
        nameF: "Pat",
        nameL: "Donnely",
        title: "The Gazette",
        words: "Over 32 days following the arrival of the stowaways, everyone descends deeper and deeper into Arctic hell, within a beautifully designed, corrugated metal shack that doubles as a classroom and a living space. ...the performaces are admirably ernest and energetic. Manning and Davis, in particular, are often riveting.",
        dates: "October 14, 2009",
        links: "2009-rpjk-gazette.pdf",
      },
      cri05: {
        nameF: "Anna",
        nameL: "Fuerstenberg",
        title: "Rover Arts",
        words: "This is a play which goes to a very dark place in human experience and it resonates with the worst and most savage results there. It is an edgy and very postmodern work which challenges the audience to follow the drama down to its horrible conclusion. It was Lord of the Flies on ice with alcohol and glue sniffing thrown into the mix.",
        dates: "October 12, 2009",
        links: "2009-rpjk-rover.pdf",
      },
      cri06: {
        nameF: "Enrico",
        nameL: "Quilico",
        title: "The Concordian",
        words: "<em>Rock, Paper, Jackknife...</em> ...will have you twisting in your seat as playwright Marilyn Perreault explores the perversion of the human mind and the extremes it is capable of ...the play has a profound message about solitude and the human mind. Originally written in French, the play is masterfully translated by Nadine Desrochers and well directed by Emma Tibaldo.",
        dates: "October 13, 2009",
        links: "2009-rpjk-concordian.pdf",
      },
    },
    logos: {
      log01: {
        image: "CCFA_RGB_colour_e.jpg",
      },
      log02: {
        image: "Calq_cyan.jpg",
      },
      log03: {
        image: "logo-cole.jpg",
      },
      log04: {
        image: "CMYK_Logo_CAM+Montreal.jpg",
      },
    },
  },
  {
    pyear: "2008",
    dateE: "November 6-15, 2008",
    dateF: "6-15 novembre, 2008",
    place: "La Chapelle Theatre",
    title: "Down Dangerous Passes Road",
    image: "poster-2008_ddpr.jpg",
    bannE: "banner-2008_en_ddpr.jpg",
    bannF: "banner-2008_fr_ddpr.jpg",
    synop: {
      texts: {
        par01: "Carl has returned to Alma to get married fifteen years to the day since their father, a drunk and a poet, drowned. On the morning of the wedding Victor takes his brothers on a trip to his isolated fishing camp in the Quebec woods. On the way they have a tragic truck-wreck near where their father died. <em>Down Dangerous Passes Road</em> traces the consequences of this event.",
        par02: "<em>Down Dangerous Passes Road</em> was written in 1998, it is Bouchard's ninth play. In 2000 it was translated by Linda Gaboriau. This is the story of three brothers who come from vastly different walks of life.The play takes place in an eternal moment of deja-vu and examines three lives struggling for a final moment of lucidity. This is a frank play about guilt and death; as Ambrose says: Nobody's as frank as a dying man.The story unwinds on a logging road in a strange limbo where there are no mosquitos. In a state of delayed shock after an accident, they reenact the trauma of their father's death, each from his own perspective.",
        par03: "In his Prologue to <em>Down Dangerous Passes Road</em>, Bouchard draws our attention to the way we hide from the realities of guilt and death, protecting ourselves behind illusions and routines. The play shows how the complicated games of lies and truth we play in our relationships can precipitate us into unforseen situations. With simple words, and in a simple setting, Bouchard evokes a gripping, multi-layered, allegorical commentary on Quebec culture through three brothers bound together by the death of their father. ",
      },
    },
    auths: {
      aut01: {
        nameF: "Michel Marc",
        nameL: "Bouchard",
      },
    },
    trans: {
      tra01: {
        nameF: "Linda",
        nameL: "Gaboriau",
      },
    },
    drama: {
      dra01: {
        nameF: "",
        nameL: "",
      },
    },
    direc: {
      dir01: {
        nameF: "Emma",
        nameL: "Tibaldo",
      },
    },
    chore: {
      cho01: {
        nameF: "",
        nameL: "",
      },
    },
    actor: {
      act01: {
        nameF: "Marcelo",
        nameL: "Arroyo",
        chara: "Ambrose",
      },
      act02: {
        nameF: "Patrick",
        nameL: "Costello",
        chara: "Carl",
      },
      act03: {
        nameF: "Graham",
        nameL: "Cuthbertson",
        chara: "Victor",
      },
    },
    desig: {
      des01: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Set",
        titlF: "Décor",
      },
      des01: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Costumes",
        titlF: "Costume",
      },
      des01: {
        nameF: "David",
        nameL: "Perreault Ninacs",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des01: {
        nameF: "Sarah",
        nameL: "Yaffe",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des01: {
        nameF: "Michael",
        nameL: "Leon",
        titlE: "Sound",
        titlF: "Son",
      },
      des01: {
        nameF: "Mireille",
        nameL: "Couture",
        titlE: "Video",
        titlF: "Vidéo",
      },
      des01: {
        nameF: "Rasili",
        nameL: "Botz",
        titlE: "Movement",
        titlF: "Mouvement",
      },
    },
    copro: {
      texts: {
        par01: "A Talisman Theatre Equity Coop Production with the assistance of the Canada Council for the Arts and the Department of Canadian Heritage through IPOLC.",
      },
    },
    photo: {
      pho01: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2008-ddpr_007.jpg",
      },
      pho02: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2008-ddpr_DSC1569.jpg",
      },
      pho03: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2008-down-01.jpg",
      },
      pho04: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2008-down-02.jpg",
      },
      pho05: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2008-down-06.jpg",
      },
    },
    criti: {
      cri01: {
        nameF: "",
        nameL: "",
        title: "Talisman Theatre",
        words: "We are pleased to announce that our English-language premiere of Michel Marc Bouchard's <em>Down Dangerous Passes Road</em>, translated by Linda Gaboriau, won the 2008 ACQT prize in the category 'Théâtre anglophone.'",
        dates: "October 25, 2009",
        links: "2008-ddpr-prix-voir.pdf",
      },
      cri02: {
        nameF: "Christian",
        nameL: "Saint-Pierre",
        title: "AQCT",
        words: "D'une appréciable sobriété, ce spectacle s'appuyait avant tout sur le jeu de l'acteur. Graham Cuthbertson, Marcelo Arroyo et Patrick Costello ont livré des interprétations sensibles et nuancées. Dans une scénographie aussi dépouillée qu'évocatrice, le moindre de leurs déplacements était chargé de sens. En se mesurant dans la langue de Shakespeare aux auteurs francophones contemporains du Québec, et qui plus est avec doigté, le Talisman Theatre pourrait bien contribuer à réunir les deux solitudes.",
        dates: "November 03, 2009",
        links: "2008-ddpr-prix-01.pdf",
      },
      cri03: {
        nameF: "Pat",
        nameL: "Donnely",
        title: "The Gazette",
        words: "Talisman Theatre won the Anglophone Theatre Prix de la critique award of the 2008-2009 season for its production of Michel-Marc Bouchard's <em>Down Dangerous Passes Road</em>, directed by Emma Tibaldo at Théâtre La Chapelle. It prevailed over Wajdi Mouawad's Scorched and Bryden MacDonald's With Baited Breath, both seen at Centaur Theatre.",
        dates: "November 03, 2009",
        links: "",
      },
      cri04: {
        nameF: "Pat",
        nameL: "Donnely",
        title: "The Gazette",
        words: "As automobile accident plays go, Michel Marc Bouchard's <em>Down Dangerous Passes Road</em> rises above, into the ethereal. And the Talisman Theatre production of this poetic work, translated into English by Linda Gaboriau... does an admirable job of bridging the cultural gap that often hinders French-language Quebec plays from making a smooth transition into English [...] With this kind of play, less is more when it comes to staging. Lyne Paquette's set, which consists of giant sheets of parchment, with words scrawled in longhand, is just right. These 'pages' serve as screens for cinematic images that enhance the intense dramatic confrontations. In some ways this modest effort outshines the original French production... [...] Director Emma Tibaldo has delivered a meaningful introduction to a lesser work by the playwright...",
        dates: "November 14, 2008",
        links: "2008-ddpr-gazette.pdf",
      },
      cri05: {
        nameF: "Alexandre",
        nameL: "Cadieux",
        title: "Le Devoir",
        words: "<em>Down Dangerous Passes Road</em> is not one of Bouchard's greatest works, like Les Feluettes or Les Muses orphelines. The sense of strangeness that emanates from this drama, whose characters seem to live in a perpetual déjà vu, is not without interest. Linda Gaboriau's fine translation serves up the realistic language, interspersed with more poetic fragments, of the Lac-Saint-Jean-born author.",
        dates: "November 13, 2008",
        links: "2008-ddpr-ledevoir.pdf",
      },
      cri06: {
        nameF: "Marianne",
        nameL: "Ackerman",
        title: "Rover Arts",
        words: "Watching the clash of two very different theatre aesthetics--both the highs and the lows--makes for a fascinating 90 minutes. [...] Director Emma Tibaldo is most at ease with the naturalist elements offered by the text. The actors are riveted to each other, devouring their linesand spitting them back out with tremendous energy. [...] It won't be giving away the ending to report that <em>Down Dangerous Passes Road</em> is a cri de coeur for the sacred superiority of art, because at some level, that's what most contemporary Quebecois plays are about. Talisman's attack is bold and imaginative. Watching the dueling aesthetics, one has the feeling the effort of translation is in equal measure hopeless and essential, and therefore, quite a good reason for doing theatre.",
        dates: "November 14, 2008",
        links: "2008-ddpr-rover.pdf",
      },
    },
    logos: {
      log01: {
        image: "CCFA_RGB_colour_e.jpg",
      },
      log02: {
        image: "Calq_cyan.jpg",
      },
      log03: {
        image: "logo-cole.jpg",
      },
      log04: {
        image: "CMYK_Logo_CAM+Montreal.jpg",
      },
    },
  },
  {
    pyear: "2007",
    dateE: "September 13-27, 2007",
    dateF: "13-27 septembre, 2007",
    place: "Bain St-Michel.",
    title: "That Woman (remount)",
    image: "poster-2006_woman1.jpg",
    bannE: "banner-2007_en_woman2.jpg",
    bannF: "banner-2007_fr_woman2.jpg",
    synop: {
      texts: {
        par01: "In a series of twenty-four short scenes or snapshots, <em>That Woman</em> breathtakingly transports you into a landscape of memories where an Old Man, a Mother and a Son struggle to experience perfect moments of happiness in an existence devoid of opportunity. They collect small victories among the revelations of pain. A world opens, invites us in and asks us to participate: the body starved of knowledge and culture, the forbidden garden of sexual expression, the separation of body and soul and the inability to participate in a child's game of laughter.",
        par02: "<em>That Woman</em> is a challenge. It is written in poetic monologue form. The monologues are moments in time, told by three people that live in the same house but are not equipped to live in the same time or space. In the play, the monologues become a dialogue among the three; a composite story of repression, neglect, cruelty and astonishing resilience, love and hope. The Mother's words are spoken during the twenty minutes prior to her death. The Old Man speaks at two different times, during the night of The Mother's death while her body lies on the floor, and at the time of the Son's arrival. The Son speaks at his arrival in the house on the third day following the Mother's death. Her body is no longer in the house. The funeral for the Mother will take place the following day.",
        par03: "<em>Celle-là</em> by Daniel Danis was awarded the Governor General Award in 1993. In 1998 it was translated by Linda Gaboriau and entitled <em>That Woman</em>. This is a play that tells the story of a woman, her name never mentioned, who is sent away from her family at the age of seventeen by her brother the Bishop after she is found exploring her sexuality.",
        par04: "'A style that deftly blends the levels, weaves the voices, short-circuits the emotions, with the apparent confusion of a mental journey, creating a remarkably efficient and orderly whole.' Robert Lévesque, Le Devoir, January 15, 1993.",
      },
    },
    auths: {
      aut01: {
        nameF: "Daniel",
        nameL: "Danis",
      },
    },
    trans: {
      tra01: {
        nameF: "Linda",
        nameL: "Gaboriau",
      },
    },
    drama: {
      dra01: {
        nameF: "",
        nameL: "",
      },
    },
    direc: {
      dir01: {
        nameF: "Emma",
        nameL: "Tibaldo",
      },
    },
    chore: {
      cho01: {
        nameF: "",
        nameL: "",
      },
    },
    actor: {
      act01: {
        nameF: "Sarah",
        nameL: "Stanley",
        chara: "The Mother",
      },
      act02: {
        nameF: "Guy",
        nameL: "Sprung",
        chara: "The Old Man",
      },
      act03: {
        nameF: "Marcelo",
        nameL: "Arroyo",
        chara: "The Son",
      },
    },
    desig: {
      des01: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Set",
        titlF: "Décor",
      },
      des02: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Costumes",
        titlF: "Costume",
      },
      des03: {
        nameF: "David",
        nameL: "Perreault Ninacs",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des04: {
        nameF: "Michael",
        nameL: "Leon",
        titlE: "Sound",
        titlF: "Son",
      },
    },
    copro: {
      texts: {
        par01: "A Talisman Theatre and Infinitheatre Co-production, with the assistance of the Canada Council for the Arts and the Department of Canadian Heritage through IPOLC.",
      },
    },
    photo: {
      pho01: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2006-tw-DSCN0121.JPG",
      },
      pho02: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2006-tw-DSCN0128.jpg",
      },
      pho03: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2006-tw-DSCN0133.jpg",
      },
    },
    criti: {
      cri01: {
        nameF: "",
        nameL: "",
        title: " ",
        words: "We are pleased to announce that our English-language premiere of Daniel Danis's <em>That Woman</em>, translated by Linda Gaboriau, has been nominated for the 2007 ACQT prize in the category 'Théâtre anglophone.'",
        dates: "October 28, 2008",
        links: "2007-tw-mirror.pdf",
      },
      cri02: {
        nameF: "Brett",
        nameL: "Hooton",
        title: "The Mirror",
        words: "Emma Tibaldo's dream has always been to find the perfect combination of English and French theatre. [...] Tibaldo insists... that this 'brutal and hopeful and beautiful and ugly and incredibly poetic' play will be distinct from its previous incarnation. 'It's not a remount for me', she says. 'It's a deepening of the work. it's exploring where we didn't dare explore last time. And the deeper you go, the more it hurts, but the more rewarding it is also. [...] We live in the same city. We share the same services. We share the same music. We share the streets, the metro. You know, we share everything. We share our lives. To not share our theatre seems insane to me.'",
        dates: "October 28, 2008",
        links: "2007-tw-mirror.pdf",
      },
    },
    logos: {
      log01: {
        image: "CCFA_RGB_colour_e.jpg",
      },
      log02: {
        image: "Calq_cyan.jpg",
      },
      log03: {
        image: "logo-cole.jpg",
      },
      log04: {
        image: "CMYK_Logo_CAM+Montreal.jpg",
      },
    },
  },
  {
    pyear: "2006",
    dateE: "September 20-30, 2006",
    dateF: "20-30 septembre, 2006",
    place: "Geordie Space",
    title: "That Woman",
    image: "poster-2007_woman2.jpg",
    bannE: "banner-2006_en_woman1.jpg",
    bannF: "banner-2006_fr_woman1.jpg",
    synop: {
      texts: {
        par01: "In a series of twenty-four short scenes or snapshots, <em>That Woman</em> breathtakingly transports you into a landscape of memories where an Old Man, a Mother and a Son struggle to experience perfect moments of happiness in an existence devoid of opportunity. They collect small victories among the revelations of pain. A world opens, invites us in and asks us to participate: the body starved of knowledge and culture, the forbidden garden of sexual expression, the separation of body and soul and the inability to participate in a child's game of laughter.",
        par02: "<em>That Woman</em> is a challenge. It is written in poetic monologue form. The monologues are moments in time, told by three people that live in the same house but are not equipped to live in the same time or space. In the play, the monologues become a dialogue among the three; a composite story of repression, neglect, cruelty and astonishing resilience, love and hope. The Mother's words are spoken during the twenty minutes prior to her death. The Old Man speaks at two different times, during the night of The Mother's death while her body lies on the floor, and at the time of the Son's arrival. The Son speaks at his arrival in the house on the third day following the Mother's death. Her body is no longer in the house. The funeral for the Mother will take place the following day.",
        par03: "<em>Celle-là</em> by Daniel Danis was awarded the Governor General Award in 1993. In 1998 it was translated by Linda Gaboriau and entitled <em>That Woman</em>. This is a play that tells the story of a woman, her name never mentioned, who is sent away from her family at the age of seventeen by her brother the Bishop after she is found exploring her sexuality.",
        par04: "'A style that deftly blends the levels, weaves the voices, short-circuits the emotions, with the apparent confusion of a mental journey, creating a remarkably efficient and orderly whole.' Robert Lévesque, Le Devoir, January 15, 1993.",
      },
    },
    auths: {
      aut01: {
        nameF: "Daniel",
        nameL: "Danis",
      },
    },
    trans: {
      tra01: {
        nameF: "Linda",
        nameL: "Gaboriau",
      },
    },
    drama: {
      dra01: {
        nameF: "",
        nameL: "",
      },
    },
    direc: {
      dir01: {
        nameF: "Emma",
        nameL: "Tibaldo",
      },
    },
    chore: {
      cho01: {
        nameF: "",
        nameL: "",
      },
    },
    actor: {
      act01: {
        nameF: "Sarah",
        nameL: "Stanley",
        chara: "The Mother",
      },
      act02: {
        nameF: "Guy",
        nameL: "Sprung",
        chara: "The Old Man",
      },
      act03: {
        nameF: "Marcelo",
        nameL: "Arroyo",
        chara: "The Son",
      },
    },
    desig: {
      des01: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Set",
        titlF: "Décor",
      },
      des02: {
        nameF: "Lyne",
        nameL: "Paquette",
        titlE: "Costumes",
        titlF: "Costume",
      },
      des03: {
        nameF: "David",
        nameL: "Perreault Ninacs",
        titlE: "Lighting",
        titlF: "Éclairage",
      },
      des04: {
        nameF: "Michael",
        nameL: "Leon",
        titlE: "Sound",
        titlF: "Son",
      },
    },
    copro: {
      texts: {
        par01: "A Talisman Theatre and Infinitheatre Co-production, with the assistance of the Canada Council for the Arts and the Department of Canadian Heritage through IPOLC.",
      },
    },
    photo: {
      pho01: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2007-That_Woman026.jpg",
      },
      pho02: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2007-That_Woman037.jpg",
      },
      pho03: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2007-That_Woman043.jpg",
      },
      pho04: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2007-that2-02.jpg",
      },
      pho05: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2007-that2-06.jpg",
      },
      pho06: {
        nameF: "Chris",
        nameL: "Dilworth",
        image: "2007-tw-That_Woman023.jpg",
      },
    },
    criti: {
      cri01: {
        nameF: "Mat",
        nameL: "Radz",
        title: "The Gazette",
        words: "Something sinister infects the sordid story taking shape in spasmodic out-of-sync fits and starts of high and low emotions. [...] Directed by Emma Tibaldo, the production unites a group of talented veterans under the umbrella of Talisman Theatre, a cooperative formed to put on Quebec plays not yet seen in Montreal in English. Good news indeed, and it comes not a moment too soon to help fill a gap in the pure-laine anglo playgoing experience.",
        dates: "September 22, 2006",
        links: "/assets/docs/2006-tw-gazette.pdf",
      },
      cri02: {
        nameF: "Amy",
        nameL: "Barratt",
        title: "The Mirror",
        words: "[D]ozens of new French plays come out each year, and then there's all the stuff from past years that never got picked up for English language production. That's where <em>That Woman</em> and Talisman productions come in. As indie theatre companies go, this one is fairly high-powered. It was started by hot young director Emma Tibaldo and fellow NTS graduate, designer Lyne Paquette.",
        dates: "",
        links: "/assets/docs/2006-tw-mirror.pdf",
      },
      cri03: {
        nameF: "Jodi",
        nameL: "Essery",
        title: "The Hour",
        words: "At its wiry core, <em>That Woman</em> is a story about resilience, told by a triangle of characters who live dirty; who, because of their place at the bottom of the pile, are allowed to express life's depths, and in doing so, see its heights in a particularly illuminating way. As a reflection of clenched emotions playwright Daniel Danis's characters experience, the language, the style and the sparseness of the piece are its greatest assets.",
        dates: "September 28, 2006",
        links: "/assets/assets/docs/2006-tw-hour.pdf",
      },
    },
    logos: {
      log01: {
        image: "CCFA_RGB_colour_e.jpg",
      },
      log02: {
        image: "Calq_cyan.jpg",
      },
      log03: {
        image: "logo-cole.jpg",
      },
      log04: {
        image: "CMYK_Logo_CAM+Montreal.jpg",
      },
    },
  },
]

module.exports = pastps