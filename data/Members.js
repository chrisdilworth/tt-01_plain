

const members = [
  {
    id: 1,
    name: 'John Doe',
    email: 'john@email.com',
    status: 'Active'
  },
  {
    id: 2,
    name: 'Bob Williams',
    email: 'bob@email.com',
    status: 'Inactive'
  },
  {
    id: 3,
    name: 'Shannon Jackson',
    email: 'shannon@email.com',
    status: 'Active'
  }
]

module.exports = members